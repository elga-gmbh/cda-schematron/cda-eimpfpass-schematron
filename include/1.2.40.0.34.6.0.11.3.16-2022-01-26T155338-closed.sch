<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.16
Name: Antikörper-Bestimmung Laboratory Observation Entry
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338-closed">
   <title>Antikörper-Bestimmung Laboratory Observation Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']])]"
         id="d42e32842-true-d1246897e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e32842-true-d1246897e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']] (rule-reference: d42e32842-true-d1246897e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.13-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text | self::hl7:statusCode[@code = 'completed' or @code = 'aborted'] | self::hl7:effectiveTime[not(@nullFlavor)] | self::hl7:effectiveTime[@nullFlavor='UNK'] | self::hl7:value[@xsi:type='PQ'] | self::hl7:value[@xsi:type='IVL_PQ'] | self::hl7:value[@xsi:type='INT'] | self::hl7:value[@xsi:type='IVL_INT'] | self::hl7:value[@xsi:type='BL'] | self::hl7:value[@xsi:type='ST'] | self::hl7:value[@xsi:type='CV'] | self::hl7:value[@xsi:type='TS'] | self::hl7:value[@xsi:type='CD'] | self::hl7:value[@xsi:type='RTO'] | self::hl7:value[@xsi:type='RTO_QTY_QTY'] | self::hl7:value[@xsi:type='RTO_PQ_PQ'] | self::hl7:interpretationCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.13-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']] | self::hl7:participant[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.5']] | self::hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']] | self::hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']] | self::hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']] | self::hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]] | self::hl7:referenceRange[@typeCode = 'REFV'][hl7:observationRange[@classCode = 'OBS'][@moodCode = 'EVN.CRT']])]"
         id="d42e32933-true-d1247533e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e32933-true-d1247533e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6'] | hl7:id[not(@nullFlavor)] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.13-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text | hl7:statusCode[@code = 'completed' or @code = 'aborted'] | hl7:effectiveTime[not(@nullFlavor)] | hl7:effectiveTime[@nullFlavor='UNK'] | hl7:value[@xsi:type='PQ'] | hl7:value[@xsi:type='IVL_PQ'] | hl7:value[@xsi:type='INT'] | hl7:value[@xsi:type='IVL_INT'] | hl7:value[@xsi:type='BL'] | hl7:value[@xsi:type='ST'] | hl7:value[@xsi:type='CV'] | hl7:value[@xsi:type='TS'] | hl7:value[@xsi:type='CD'] | hl7:value[@xsi:type='RTO'] | hl7:value[@xsi:type='RTO_QTY_QTY'] | hl7:value[@xsi:type='RTO_PQ_PQ'] | hl7:interpretationCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.13-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']] | hl7:participant[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.5']] | hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']] | hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']] | hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']] | hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]] | hl7:referenceRange[@typeCode = 'REFV'][hl7:observationRange[@classCode = 'OBS'][@moodCode = 'EVN.CRT']] (rule-reference: d42e32933-true-d1247533e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1247560e54-true-d1247572e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1247560e54-true-d1247572e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1247560e54-true-d1247572e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:value[@xsi:type='PQ']/*[not(@xsi:nil = 'true')][not(self::hl7:translation)]"
         id="d42e33036-true-d1247601e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e33036-true-d1247601e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:translation (rule-reference: d42e33036-true-d1247601e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.28'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7'] | self::hl7:time[not(@nullFlavor)] | self::hl7:assignedEntity[not(@nullFlavor)])]"
         id="d42e33106-true-d1247805e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e33106-true-d1247805e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.28'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7'] | hl7:time[not(@nullFlavor)] | hl7:assignedEntity[not(@nullFlavor)] (rule-reference: d42e33106-true-d1247805e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:code[@nullFlavor='UNK'] | self::hl7:addr[not(@nullFlavor)] | self::hl7:addr[@nullFlavor='UNK'] | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1247652e27-true-d1247904e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1247652e27-true-d1247904e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:code[@nullFlavor='UNK'] | hl7:addr[not(@nullFlavor)] | hl7:addr[@nullFlavor='UNK'] | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1247652e27-true-d1247904e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1247908e127-true-d1247984e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.29-2021-06-28T134423.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBodyWithNameAddrAndTelecom)/d1247908e127-true-d1247984e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1247908e127-true-d1247984e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1247908e197-true-d1248063e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.29-2021-06-28T134423.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBodyWithNameAddrAndTelecom)/d1247908e197-true-d1248063e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1247908e197-true-d1248063e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1247908e214-true-d1248085e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.29-2021-06-28T134423.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBodyWithNameAddrAndTelecom)/d1247908e214-true-d1248085e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1247908e214-true-d1248085e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1248073e16-true-d1248114e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.29-2021-06-28T134423.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBodyWithNameAddrAndTelecom)/d1248073e16-true-d1248114e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1248073e16-true-d1248114e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d1247908e231-true-d1248199e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1247908e231-true-d1248199e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d1247908e231-true-d1248199e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.28'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1248139e70-true-d1248266e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1248139e70-true-d1248266e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1248139e70-true-d1248266e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.5']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.5'] | self::hl7:time | self::hl7:participantRole[hl7:playingEntity])]"
         id="d42e33112-true-d1248333e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e33112-true-d1248333e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.5'] | hl7:time | hl7:participantRole[hl7:playingEntity] (rule-reference: d42e33112-true-d1248333e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.5']]/hl7:participantRole[hl7:playingEntity]/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:addr | self::hl7:telecom | self::hl7:playingEntity[not(@nullFlavor)])]"
         id="d42e33129-true-d1248373e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e33129-true-d1248373e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:addr | hl7:telecom | hl7:playingEntity[not(@nullFlavor)] (rule-reference: d42e33129-true-d1248373e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.5']]/hl7:participantRole[hl7:playingEntity]/hl7:playingEntity[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d42e33139-true-d1248402e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e33139-true-d1248402e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d42e33139-true-d1248402e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44'] | self::hl7:time[not(@nullFlavor)] | self::hl7:participantRole[not(@nullFlavor)])]"
         id="d42e33150-true-d1248482e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e33150-true-d1248482e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44'] | hl7:time[not(@nullFlavor)] | hl7:participantRole[not(@nullFlavor)] (rule-reference: d42e33150-true-d1248482e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']]/hl7:participantRole[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:playingDevice | self::hl7:playingEntity | self::hl7:scopingEntity)]"
         id="d1248412e67-true-d1248532e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1248412e67-true-d1248532e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:playingDevice | hl7:playingEntity | hl7:scopingEntity (rule-reference: d1248412e67-true-d1248532e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']]/hl7:participantRole[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1248412e96-true-d1248591e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1248412e96-true-d1248591e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1248412e96-true-d1248591e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']]/hl7:participantRole[not(@nullFlavor)]/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1248412e126-true-d1248656e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1248412e126-true-d1248656e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1248412e126-true-d1248656e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']]/hl7:participantRole[not(@nullFlavor)]/hl7:playingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1248412e128-true-d1248680e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1248412e128-true-d1248680e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1248412e128-true-d1248680e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']]/hl7:participantRole[not(@nullFlavor)]/hl7:playingEntity/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1248412e134-true-d1248709e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1248412e134-true-d1248709e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1248412e134-true-d1248709e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.44']]/hl7:participantRole[not(@nullFlavor)]/hl7:scopingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:desc)]"
         id="d1248412e233-true-d1248743e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1248412e233-true-d1248743e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:desc (rule-reference: d1248412e233-true-d1248743e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46'] | self::hl7:participantRole[not(@nullFlavor)])]"
         id="d42e33206-true-d1248773e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e33206-true-d1248773e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46'] | hl7:participantRole[not(@nullFlavor)] (rule-reference: d42e33206-true-d1248773e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']]/hl7:participantRole[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)][not(@nullFlavor)])]"
         id="d1248753e38-true-d1248789e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1248753e38-true-d1248789e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)][not(@nullFlavor)] (rule-reference: d1248753e38-true-d1248789e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47'] | self::hl7:time | self::hl7:participantRole[not(@nullFlavor)])]"
         id="d42e33240-true-d1248867e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e33240-true-d1248867e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47'] | hl7:time | hl7:participantRole[not(@nullFlavor)] (rule-reference: d42e33240-true-d1248867e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']]/hl7:participantRole[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:playingDevice | self::hl7:playingEntity | self::hl7:scopingEntity)]"
         id="d1248797e45-true-d1248917e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1248797e45-true-d1248917e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:playingDevice | hl7:playingEntity | hl7:scopingEntity (rule-reference: d1248797e45-true-d1248917e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']]/hl7:participantRole[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1248797e74-true-d1248976e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1248797e74-true-d1248976e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1248797e74-true-d1248976e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']]/hl7:participantRole[not(@nullFlavor)]/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1248797e104-true-d1249041e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1248797e104-true-d1249041e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1248797e104-true-d1249041e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']]/hl7:participantRole[not(@nullFlavor)]/hl7:playingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1248797e106-true-d1249065e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1248797e106-true-d1249065e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1248797e106-true-d1249065e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']]/hl7:participantRole[not(@nullFlavor)]/hl7:playingEntity/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1248797e112-true-d1249094e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1248797e112-true-d1249094e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1248797e112-true-d1249094e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.47']]/hl7:participantRole[not(@nullFlavor)]/hl7:scopingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:desc)]"
         id="d1248797e217-true-d1249128e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1248797e217-true-d1249128e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:desc (rule-reference: d1248797e217-true-d1249128e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']])]"
         id="d42e33275-true-d1249609e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e33275-true-d1249609e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']] (rule-reference: d42e33275-true-d1249609e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[(@code = '48767-8' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:participant[@typeCode][hl7:participantRole])]"
         id="d1249138e17-true-d1250129e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1249138e17-true-d1250129e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2'] | hl7:id[not(@nullFlavor)] | hl7:code[(@code = '48767-8' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:participant[@typeCode][hl7:participantRole] (rule-reference: d1249138e17-true-d1250129e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1250160e54-true-d1250172e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1250160e54-true-d1250172e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1250160e54-true-d1250172e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | self::hl7:time | self::hl7:assignedEntity[not(@nullFlavor)])]"
         id="d1249138e87-true-d1250327e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1249138e87-true-d1250327e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | hl7:time | hl7:assignedEntity[not(@nullFlavor)] (rule-reference: d1249138e87-true-d1250327e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1250188e14-true-d1250433e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1250188e14-true-d1250433e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1250188e14-true-d1250433e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1250437e101-true-d1250506e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1250437e101-true-d1250506e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1250437e101-true-d1250506e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1250437e174-true-d1250582e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1250437e174-true-d1250582e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1250437e174-true-d1250582e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1250437e186-true-d1250604e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1250437e186-true-d1250604e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1250437e186-true-d1250604e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1250592e12-true-d1250633e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1250592e12-true-d1250633e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1250592e12-true-d1250633e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1250437e198-true-d1250684e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1250437e198-true-d1250684e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1250437e198-true-d1250684e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1250658e58-true-d1250745e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1250658e58-true-d1250745e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1250658e58-true-d1250745e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1249138e90-true-d1250912e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1249138e90-true-d1250912e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1249138e90-true-d1250912e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1250790e45-true-d1251020e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1250790e45-true-d1251020e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1250790e45-true-d1251020e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1250790e64-true-d1251079e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1250790e64-true-d1251079e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1250790e64-true-d1251079e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1250790e110-true-d1251136e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1250790e110-true-d1251136e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1250790e110-true-d1251136e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1251140e92-true-d1251170e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1251140e92-true-d1251170e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1251140e92-true-d1251170e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1250790e133-true-d1251218e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1250790e133-true-d1251218e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1250790e133-true-d1251218e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1250790e149-true-d1251263e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1250790e149-true-d1251263e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1250790e149-true-d1251263e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1251233e67-true-d1251326e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1251233e67-true-d1251326e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1251233e67-true-d1251326e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1249138e96-true-d1251494e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1249138e96-true-d1251494e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1249138e96-true-d1251494e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1251371e9-true-d1251626e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1251371e9-true-d1251626e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1251371e9-true-d1251626e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1251499e50-true-d1251692e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1251499e50-true-d1251692e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1251499e50-true-d1251692e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1251499e120-true-d1251754e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1251499e120-true-d1251754e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1251499e120-true-d1251754e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1251499e132-true-d1251776e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1251499e132-true-d1251776e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1251499e132-true-d1251776e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1251764e12-true-d1251805e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1251764e12-true-d1251805e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1251764e12-true-d1251805e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1251499e143-true-d1251856e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1251499e143-true-d1251856e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1251499e143-true-d1251856e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1251830e58-true-d1251917e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1251830e58-true-d1251917e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1251830e58-true-d1251917e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1251371e11-true-d1251994e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1251371e11-true-d1251994e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1251371e11-true-d1251994e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1251371e20-true-d1252047e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1251371e20-true-d1252047e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1251371e20-true-d1252047e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1251371e24-true-d1252102e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1251371e24-true-d1252102e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1251371e24-true-d1252102e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1252095e10-true-d1252129e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1252095e10-true-d1252129e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1252095e10-true-d1252129e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/*[not(@xsi:nil = 'true')][not(self::hl7:time | self::hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:participantRole)]"
         id="d1249138e102-true-d1252257e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1249138e102-true-d1252257e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time | hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:participantRole (rule-reference: d1249138e102-true-d1252257e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:playingDevice | self::hl7:playingEntity | self::hl7:scopingEntity)]"
         id="d1252160e11-true-d1252365e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1252160e11-true-d1252365e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:addr | hl7:telecom | hl7:playingDevice | hl7:playingEntity | hl7:scopingEntity (rule-reference: d1252160e11-true-d1252365e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1252160e22-true-d1252427e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1252160e22-true-d1252427e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1252160e22-true-d1252427e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:manufacturerModelName | self::hl7:softwareName)]"
         id="d1252160e78-true-d1252505e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1252160e78-true-d1252505e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:manufacturerModelName | hl7:softwareName (rule-reference: d1252160e78-true-d1252505e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:quantity | self::hl7:name | self::sdtc:birthTime | self::hl7:desc)]"
         id="d1252160e80-true-d1252566e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1252160e80-true-d1252566e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:quantity | hl7:name | sdtc:birthTime | hl7:desc (rule-reference: d1252160e80-true-d1252566e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:scopingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:desc)]"
         id="d1252160e83-true-d1252624e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d1252160e83-true-d1252624e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:desc (rule-reference: d1252160e83-true-d1252624e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:referenceRange[@typeCode = 'REFV'][hl7:observationRange[@classCode = 'OBS'][@moodCode = 'EVN.CRT']]/*[not(@xsi:nil = 'true')][not(self::hl7:observationRange[hl7:interpretationCode[not(@nullFlavor)]])]"
         id="d42e33295-true-d1252658e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e33295-true-d1252658e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observationRange[hl7:interpretationCode[not(@nullFlavor)]] (rule-reference: d42e33295-true-d1252658e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:referenceRange[@typeCode = 'REFV'][hl7:observationRange[@classCode = 'OBS'][@moodCode = 'EVN.CRT']]/hl7:observationRange[hl7:interpretationCode[not(@nullFlavor)]]/*[not(@xsi:nil = 'true')][not(self::hl7:text[not(@nullFlavor)] | self::hl7:value[@xsi:type='IVL_PQ'] | self::hl7:value[@xsi:type='RTO'] | self::hl7:interpretationCode[not(@nullFlavor)][not(@nullFlavor)])]"
         id="d42e33302-true-d1252674e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e33302-true-d1252674e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:text[not(@nullFlavor)] | hl7:value[@xsi:type='IVL_PQ'] | hl7:value[@xsi:type='RTO'] | hl7:interpretationCode[not(@nullFlavor)][not(@nullFlavor)] (rule-reference: d42e33302-true-d1252674e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:referenceRange[@typeCode = 'REFV'][hl7:observationRange[@classCode = 'OBS'][@moodCode = 'EVN.CRT']]/hl7:observationRange[hl7:interpretationCode[not(@nullFlavor)]]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d42e33308-true-d1252688e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e33308-true-d1252688e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d42e33308-true-d1252688e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:referenceRange[@typeCode = 'REFV'][hl7:observationRange[@classCode = 'OBS'][@moodCode = 'EVN.CRT']]/hl7:observationRange[hl7:interpretationCode[not(@nullFlavor)]]/hl7:value[@xsi:type='IVL_PQ']/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d42e33315-true-d1252710e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e33315-true-d1252710e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d42e33315-true-d1252710e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:referenceRange[@typeCode = 'REFV'][hl7:observationRange[@classCode = 'OBS'][@moodCode = 'EVN.CRT']]/hl7:observationRange[hl7:interpretationCode[not(@nullFlavor)]]/hl7:value[@xsi:type='RTO']/*[not(@xsi:nil = 'true')][not(self::hl7:numerator[not(@nullFlavor)] | self::hl7:denominator)]"
         id="d42e33322-true-d1252737e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e33322-true-d1252737e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:numerator[not(@nullFlavor)] | hl7:denominator (rule-reference: d42e33322-true-d1252737e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.16'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:referenceRange[@typeCode = 'REFV'][hl7:observationRange[@classCode = 'OBS'][@moodCode = 'EVN.CRT']]/hl7:observationRange[hl7:interpretationCode[not(@nullFlavor)]]/hl7:value[@xsi:type='RTO']/hl7:denominator/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d42e33329-true-d1252761e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.16-2022-01-26T155338.html"
              test="not(.)">(eimpf_entry_AntikoerperBestimmungLaboratoryObservation)/d42e33329-true-d1252761e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d42e33329-true-d1252761e0)</assert>
   </rule>
</pattern>
