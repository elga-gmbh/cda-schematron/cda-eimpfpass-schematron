<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.19
Name: Eingebettetes Objekt Entry
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314-closed">
   <title>Eingebettetes Objekt Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']])]"
         id="d42e35935-true-d1320001e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d42e35935-true-d1320001e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']] (rule-reference: d42e35935-true-d1320001e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19'] | self::hl7:value[not(@nullFlavor)] | self::hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:participant[@typeCode][hl7:participantRole])]"
         id="d42e35974-true-d1320489e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d42e35974-true-d1320489e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19'] | hl7:value[not(@nullFlavor)] | hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:participant[@typeCode][hl7:participantRole] (rule-reference: d42e35974-true-d1320489e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | self::hl7:time | self::hl7:assignedEntity[not(@nullFlavor)])]"
         id="d42e36026-true-d1320644e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d42e36026-true-d1320644e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | hl7:time | hl7:assignedEntity[not(@nullFlavor)] (rule-reference: d42e36026-true-d1320644e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1320505e14-true-d1320750e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1320505e14-true-d1320750e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1320505e14-true-d1320750e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1320754e101-true-d1320823e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1320754e101-true-d1320823e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1320754e101-true-d1320823e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1320754e174-true-d1320899e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1320754e174-true-d1320899e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1320754e174-true-d1320899e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1320754e186-true-d1320921e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1320754e186-true-d1320921e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1320754e186-true-d1320921e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1320909e12-true-d1320950e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1320909e12-true-d1320950e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1320909e12-true-d1320950e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1320754e198-true-d1321001e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1320754e198-true-d1321001e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1320754e198-true-d1321001e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1320975e58-true-d1321062e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1320975e58-true-d1321062e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1320975e58-true-d1321062e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e36028-true-d1321229e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d42e36028-true-d1321229e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e36028-true-d1321229e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1321107e41-true-d1321337e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1321107e41-true-d1321337e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1321107e41-true-d1321337e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1321107e60-true-d1321396e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1321107e60-true-d1321396e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1321107e60-true-d1321396e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1321107e106-true-d1321453e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1321107e106-true-d1321453e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1321107e106-true-d1321453e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1321457e92-true-d1321487e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1321457e92-true-d1321487e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1321457e92-true-d1321487e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1321107e129-true-d1321535e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1321107e129-true-d1321535e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1321107e129-true-d1321535e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1321107e145-true-d1321580e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1321107e145-true-d1321580e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1321107e145-true-d1321580e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1321550e67-true-d1321643e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1321550e67-true-d1321643e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1321550e67-true-d1321643e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e36030-true-d1321811e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d42e36030-true-d1321811e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e36030-true-d1321811e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1321688e5-true-d1321943e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1321688e5-true-d1321943e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1321688e5-true-d1321943e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1321816e50-true-d1322009e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1321816e50-true-d1322009e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1321816e50-true-d1322009e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1321816e120-true-d1322071e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1321816e120-true-d1322071e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1321816e120-true-d1322071e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1321816e132-true-d1322093e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1321816e132-true-d1322093e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1321816e132-true-d1322093e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1322081e12-true-d1322122e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1322081e12-true-d1322122e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1322081e12-true-d1322122e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1321816e143-true-d1322173e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1321816e143-true-d1322173e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1321816e143-true-d1322173e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1322147e58-true-d1322234e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1322147e58-true-d1322234e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1322147e58-true-d1322234e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1321688e7-true-d1322311e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1321688e7-true-d1322311e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1321688e7-true-d1322311e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1321688e16-true-d1322364e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1321688e16-true-d1322364e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1321688e16-true-d1322364e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1321688e20-true-d1322419e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1321688e20-true-d1322419e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1321688e20-true-d1322419e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1322412e10-true-d1322446e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1322412e10-true-d1322446e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1322412e10-true-d1322446e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/*[not(@xsi:nil = 'true')][not(self::hl7:time | self::hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:participantRole)]"
         id="d42e36032-true-d1322574e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d42e36032-true-d1322574e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time | hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:participantRole (rule-reference: d42e36032-true-d1322574e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:playingDevice | self::hl7:playingEntity | self::hl7:scopingEntity)]"
         id="d1322477e11-true-d1322682e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1322477e11-true-d1322682e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:addr | hl7:telecom | hl7:playingDevice | hl7:playingEntity | hl7:scopingEntity (rule-reference: d1322477e11-true-d1322682e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1322477e22-true-d1322744e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1322477e22-true-d1322744e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1322477e22-true-d1322744e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:manufacturerModelName | self::hl7:softwareName)]"
         id="d1322477e78-true-d1322822e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1322477e78-true-d1322822e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:manufacturerModelName | hl7:softwareName (rule-reference: d1322477e78-true-d1322822e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:quantity | self::hl7:name | self::sdtc:birthTime | self::hl7:desc)]"
         id="d1322477e80-true-d1322883e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1322477e80-true-d1322883e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:quantity | hl7:name | sdtc:birthTime | hl7:desc (rule-reference: d1322477e80-true-d1322883e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:scopingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:desc)]"
         id="d1322477e83-true-d1322941e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.19-2021-02-19T124314.html"
              test="not(.)">(atcdabbr_entry_EingebettetesObjektEntry)/d1322477e83-true-d1322941e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:desc (rule-reference: d1322477e83-true-d1322941e0)</assert>
   </rule>
</pattern>
