<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.0.2
Name: Update Immunisierungsstatus
Description: 
                 Spezieller Implementierungsleitfaden e-Impfpass für Dokument:  Update Immunisierungsstatus  (Dokument-Level-Template). Ein Dokument enthält mindestens eine Sektion "Impfungen" und optional weitere Sektionen (Impfempfehlungen, Indikationsgruppen, Impfrelevante Erkrankungen, Antikörper-Bestimmung, Beilagen). 
            
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821">
   <title>Update Immunisierungsstatus</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <rule fpi="RULC-1" context="/" id="d42e1033-false-d2831e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]"
         id="d42e1164-false-d3864e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@classCode) = ('DOCCLIN') or not(@classCode)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von classCode MUSS 'DOCCLIN' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@moodCode) = ('EVN') or not(@moodCode)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:author/hl7:assignedAuthor/hl7:assignedAuthoringDevice)=1">(eimpf_document_UpdateImmunisierungsstatus): Es MUSS genau eine dokumenterstellende Software angeben werden.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="not(hl7:relatedDocument) or hl7:relatedDocument[@typeCode='RPLC']">(eimpf_document_UpdateImmunisierungsstatus): Wird /ClinicalDocument/relatedDocument angegeben, MUSS relatedDocument[@typeCode='RPLC'] sein.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:realmCode[@code = 'AT']) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:realmCode[@code = 'AT'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:realmCode[@code = 'AT']) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:realmCode[@code = 'AT'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:typeId[@root = '2.16.840.1.113883.1.3'][@extension = 'POCD_HD000040']) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:typeId[@root = '2.16.840.1.113883.1.3'][@extension = 'POCD_HD000040'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:typeId[@root = '2.16.840.1.113883.1.3'][@extension = 'POCD_HD000040']) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:typeId[@root = '2.16.840.1.113883.1.3'][@extension = 'POCD_HD000040'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1']) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1']) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.7.19.2']) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:templateId[@root = '1.2.40.0.34.7.19.2'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.7.19.2']) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:templateId[@root = '1.2.40.0.34.7.19.2'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2']) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2']) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2.1'][@extension = 'XDSdocumentEntry.formatCode^urn:hl7-at:eImpf:2019']) = 0">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2.1'][@extension = 'XDSdocumentEntry.formatCode^urn:hl7-at:eImpf:2019'] DARF NICHT vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:id[not(@nullFlavor)]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:id[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:id[not(@nullFlavor)]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:id[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:code[(@code = '11369-6' and @codeSystem = '2.16.840.1.113883.6.1')]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:code[(@code = '11369-6' and @codeSystem = '2.16.840.1.113883.6.1')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:code[(@code = '11369-6' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:code[(@code = '11369-6' and @codeSystem = '2.16.840.1.113883.6.1')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(sdtc:statusCode) = 0">(eimpf_document_UpdateImmunisierungsstatus): Element sdtc:statusCode DARF NICHT vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7at:terminologyDate[not(@nullFlavor)]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7at:terminologyDate[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7at:terminologyDate[not(@nullFlavor)]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7at:terminologyDate[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7at:formatCode[@codeSystem = '1.2.40.0.34.5.37']) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7at:formatCode[@codeSystem = '1.2.40.0.34.5.37'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7at:formatCode[@codeSystem = '1.2.40.0.34.5.37']) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7at:formatCode[@codeSystem = '1.2.40.0.34.5.37'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7at:practiceSettingCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.75-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7at:practiceSettingCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.75-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:effectiveTime[not(@nullFlavor)]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:effectiveTime[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:effectiveTime[not(@nullFlavor)]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:effectiveTime[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:confidentialityCode[not(@nullFlavor)]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:confidentialityCode[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:confidentialityCode[not(@nullFlavor)]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:confidentialityCode[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:languageCode[not(@nullFlavor)]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:languageCode[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:languageCode[not(@nullFlavor)]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:languageCode[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:setId[not(@nullFlavor)]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:setId[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:setId[not(@nullFlavor)]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:setId[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:versionNumber[not(@nullFlavor)]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:versionNumber[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:versionNumber[not(@nullFlavor)]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:versionNumber[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:recordTarget[not(@nullFlavor)][hl7:patientRole] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:recordTarget[not(@nullFlavor)][hl7:patientRole] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:author[not(@nullFlavor)][hl7:assignedAuthor]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:author[not(@nullFlavor)][hl7:assignedAuthor] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:dataEnterer[hl7:assignedEntity]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:dataEnterer[hl7:assignedEntity] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:custodian[not(@nullFlavor)][hl7:assignedCustodian]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:custodian[not(@nullFlavor)][hl7:assignedCustodian] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:custodian[not(@nullFlavor)][hl7:assignedCustodian]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:custodian[not(@nullFlavor)][hl7:assignedCustodian] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:informationRecipient) = 0">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:informationRecipient DARF NICHT vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:authenticator) = 0">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:authenticator DARF NICHT vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:participant) = 0">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:participant DARF NICHT vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:inFulfillmentOf) = 0">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:inFulfillmentOf DARF NICHT vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:relatedDocument[@typeCode][hl7:parentDocument]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:relatedDocument[@typeCode][hl7:parentDocument] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:authorization) = 0">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:authorization DARF NICHT vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:component[not(@nullFlavor)][hl7:structuredBody]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:component[not(@nullFlavor)][hl7:structuredBody] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:component[not(@nullFlavor)][hl7:structuredBody]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:component[not(@nullFlavor)][hl7:structuredBody] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.10
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:realmCode[@code = 'AT']
Item: (atcdabbr_header_DocumentRealm)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:realmCode[@code = 'AT']"
         id="d3918e152-false-d4762e0">
      <extends rule="CS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.10-2023-03-24T092127.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CS')">(atcdabbr_header_DocumentRealm): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.10-2023-03-24T092127.html"
              test="string(@code) = ('AT')">(atcdabbr_header_DocumentRealm): Der Wert von code MUSS 'AT' sein. Gefunden: "<value-of select="@code"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:typeId[@root = '2.16.840.1.113883.1.3'][@extension = 'POCD_HD000040']
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:typeId[@root = '2.16.840.1.113883.1.3'][@extension = 'POCD_HD000040']"
         id="d42e1188-false-d4777e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(eimpf_document_UpdateImmunisierungsstatus): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@root) = ('2.16.840.1.113883.1.3')">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von root MUSS '2.16.840.1.113883.1.3' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@extension) = ('POCD_HD000040')">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von extension MUSS 'POCD_HD000040' sein. Gefunden: "<value-of select="@extension"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="not(@extension) or string-length(@extension)&gt;0">(eimpf_document_UpdateImmunisierungsstatus): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1']
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1']"
         id="d42e1199-false-d4799e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(eimpf_document_UpdateImmunisierungsstatus): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.0.1')">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von root MUSS '1.2.40.0.34.6.0.11.0.1' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:templateId[@root = '1.2.40.0.34.7.19.2']
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:templateId[@root = '1.2.40.0.34.7.19.2']"
         id="d42e1207-false-d4814e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(eimpf_document_UpdateImmunisierungsstatus): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@root) = ('1.2.40.0.34.7.19.2')">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von root MUSS '1.2.40.0.34.7.19.2' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2']
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2']"
         id="d42e1218-false-d4829e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(eimpf_document_UpdateImmunisierungsstatus): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.0.2')">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von root MUSS '1.2.40.0.34.6.0.11.0.2' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2.1'][@extension = 'XDSdocumentEntry.formatCode^urn:hl7-at:eImpf:2019']
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2.1'][@extension = 'XDSdocumentEntry.formatCode^urn:hl7-at:eImpf:2019']"
         id="d42e1226-false-d4844e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(eimpf_document_UpdateImmunisierungsstatus): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@extension) = ('XDSdocumentEntry.formatCode^urn:hl7-at:eImpf:2019')">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von extension MUSS 'XDSdocumentEntry.formatCode^urn:hl7-at:eImpf:2019' sein. Gefunden: "<value-of select="@extension"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="not(@extension) or string-length(@extension)&gt;0">(eimpf_document_UpdateImmunisierungsstatus): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.0.2.1')">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von root MUSS '1.2.40.0.34.6.0.11.0.2.1' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']"
         id="d42e1244-false-d4865e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(eimpf_document_UpdateImmunisierungsstatus): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@root) = ('1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2')">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von root MUSS '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:id[not(@nullFlavor)]
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:id[not(@nullFlavor)]"
         id="d42e1252-false-d4879e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(eimpf_document_UpdateImmunisierungsstatus): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:code[(@code = '11369-6' and @codeSystem = '2.16.840.1.113883.6.1')]
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:code[(@code = '11369-6' and @codeSystem = '2.16.840.1.113883.6.1')]"
         id="d42e1273-false-d4890e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(eimpf_document_UpdateImmunisierungsstatus): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="@nullFlavor or (@code='11369-6' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='HISTORY OF IMMUNIZATIONS')">(eimpf_document_UpdateImmunisierungsstatus): Der Elementinhalt MUSS einer von 'code '11369-6' codeSystem '2.16.840.1.113883.6.1' displayName='HISTORY OF IMMUNIZATIONS'' sein.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:translation[(@code = '87273-9' and @codeSystem = '2.16.840.1.113883.6.1')]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:translation[(@code = '87273-9' and @codeSystem = '2.16.840.1.113883.6.1')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:translation[(@code = '87273-9' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:translation[(@code = '87273-9' and @codeSystem = '2.16.840.1.113883.6.1')] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:code[(@code = '11369-6' and @codeSystem = '2.16.840.1.113883.6.1')]/hl7:translation[(@code = '87273-9' and @codeSystem = '2.16.840.1.113883.6.1')]
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:code[(@code = '11369-6' and @codeSystem = '2.16.840.1.113883.6.1')]/hl7:translation[(@code = '87273-9' and @codeSystem = '2.16.840.1.113883.6.1')]"
         id="d42e1301-false-d4916e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(eimpf_document_UpdateImmunisierungsstatus): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@code) = ('87273-9')">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von code MUSS '87273-9' sein. Gefunden: "<value-of select="@code"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@codeSystem) = ('2.16.840.1.113883.6.1')">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von codeSystem MUSS '2.16.840.1.113883.6.1' sein. Gefunden: "<value-of select="@codeSystem"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@displayName) = ('Immunization note')">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von displayName MUSS 'Immunization note' sein. Gefunden: "<value-of select="@displayName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(eimpf_document_UpdateImmunisierungsstatus): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:title[not(@nullFlavor)]
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:title[not(@nullFlavor)]"
         id="d42e1323-false-d4941e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(eimpf_document_UpdateImmunisierungsstatus): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/sdtc:statusCode
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.46
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7at:terminologyDate
Item: (atcdabbr_header_DocumentTerminologyDate)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7at:terminologyDate"
         id="d4953e21-false-d4960e0">
      <extends rule="TS.DATE.FULL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.46-2021-02-19T110444.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_header_DocumentTerminologyDate): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.46-2021-02-19T110444.html"
              test="not(*)">(atcdabbr_header_DocumentTerminologyDate): <value-of select="local-name()"/> with datatype TS.DATE.FULL, SHOULD NOT have child elements.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7at:formatCode[@codeSystem = '1.2.40.0.34.5.37']
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7at:formatCode[@codeSystem = '1.2.40.0.34.5.37']"
         id="d42e1352-false-d4974e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(eimpf_document_UpdateImmunisierungsstatus): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="@nullFlavor or (@codeSystem='1.2.40.0.34.5.37')">(eimpf_document_UpdateImmunisierungsstatus): Der Elementinhalt MUSS einer von 'codeSystem '1.2.40.0.34.5.37'' sein.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="@code">(eimpf_document_UpdateImmunisierungsstatus): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="not(@code) or string-length(@code)&gt;0">(eimpf_document_UpdateImmunisierungsstatus): Attribute @code MUSS vom Datentyp 'st' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="@displayName">(eimpf_document_UpdateImmunisierungsstatus): Attribut @displayName MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(eimpf_document_UpdateImmunisierungsstatus): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@codeSystemName) = ('ELGA_FormatCode') or not(@codeSystemName)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von codeSystemName MUSS 'ELGA_FormatCode' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(eimpf_document_UpdateImmunisierungsstatus): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="matches(@code, '^urn:hl7-at:eImpf:2\.[0-9]+\.[0-9]+\+[0-9]{8}$')">(eimpf_document_UpdateImmunisierungsstatus): Es MUSS die neue Hauptversion v2 im Attribut code im formatCode verwendet werden.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="matches(@displayName, '^HL7 Austria e-Impfpass 2\.[0-9]+\.[0-9]+\+[0-9]{8}$')">(eimpf_document_UpdateImmunisierungsstatus): Es MUSS die neue Hauptversion v2 im Attribut displayName im formatCode verwendet werden.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.44
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7at:practiceSettingCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.75-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]
Item: (atcdabbr_header_DocumentPracticeSettingCode)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7at:practiceSettingCode[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.75-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]"
         id="d4975e71-false-d5010e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.44-2021-03-01T153720.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_header_DocumentPracticeSettingCode): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.44-2021-03-01T153720.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.10.75-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(atcdabbr_header_DocumentPracticeSettingCode): Der Elementinhalt MUSS einer von '1.2.40.0.34.10.75 ELGA_PracticeSetting (DYNAMIC)' sein.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.44-2021-03-01T153720.html"
              test="@displayName">(atcdabbr_header_DocumentPracticeSettingCode): Attribut @displayName MUSS vorkommen.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:effectiveTime
Item: (atcdabbr_header_DocumentEffectiveTime)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:effectiveTime"
         id="d5011e65-false-d5034e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.11-2023-04-11T102255.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_header_DocumentEffectiveTime): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.11-2023-04-11T102255.html"
              test="not(*)">(atcdabbr_header_DocumentEffectiveTime): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.12
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:confidentialityCode
Item: (atcdabbr_header_DocumentConfidentialityCode)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:confidentialityCode"
         id="d5035e52-false-d5048e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.12-2023-03-24T093046.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atcdabbr_header_DocumentConfidentialityCode): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.12-2023-03-24T093046.html"
              test="string(@codeSystemName) = ('HL7:Confidentiality')">(atcdabbr_header_DocumentConfidentialityCode): Der Wert von codeSystemName MUSS 'HL7:Confidentiality' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.12-2023-03-24T093046.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_header_DocumentConfidentialityCode): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.13
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:languageCode
Item: (atcdabbr_header_DocumentLanguage)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:languageCode"
         id="d5049e65-false-d5066e0">
      <extends rule="CS.LANG"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.13-2021-02-19T103653.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CS')">(atcdabbr_header_DocumentLanguage): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.13-2021-02-19T103653.html"
              test="@code">(atcdabbr_header_DocumentLanguage): Attribut @code MUSS vorkommen.</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@code),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.13-2021-02-19T103653.html"
              test="not(@code) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.10.10-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_header_DocumentLanguage): Der Wert von code MUSS gewählt werden aus Value Set '1.2.40.0.34.10.10' ELGA_LanguageCode (DYNAMIC).</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.15
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:setId
Item: (atcdabbr_header_DocumentSetIdAndVersionNumber)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:setId"
         id="d5067e78-false-d5093e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.15-2021-02-19T105714.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_header_DocumentSetIdAndVersionNumber): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.15
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:versionNumber
Item: (atcdabbr_header_DocumentSetIdAndVersionNumber)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:versionNumber"
         id="d5067e113-false-d5103e0">
      <extends rule="INT.NONNEG"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.15-2021-02-19T105714.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'INT')">(atcdabbr_header_DocumentSetIdAndVersionNumber): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:INT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.15-2021-02-19T105714.html"
              test="not(@value) or matches(@value, '^-?[1-9]\d*$|^+?\d*$')">(atcdabbr_header_DocumentSetIdAndVersionNumber): @value ist keine gültige INT Zahl <value-of select="@value"/>
      </assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.15-2021-02-19T105714.html"
              test="@value">(atcdabbr_header_DocumentSetIdAndVersionNumber): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.15-2021-02-19T105714.html"
              test="not(@value) or matches(string(@value), '^-?[1-9]\d*$|^+?\d*$')">(atcdabbr_header_DocumentSetIdAndVersionNumber): Attribute @value ist keine gültige int Zahl <value-of select="@value"/>
      </assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]"
         id="d5104e143-false-d5178e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="string(@typeCode) = ('RCT') or not(@typeCode)">(atcdabbr_header_RecordTarget_eImpfpass): Der Wert von typeCode MUSS 'RCT' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="string(@contextControlCode) = ('OP') or not(@contextControlCode)">(atcdabbr_header_RecordTarget_eImpfpass): Der Wert von contextControlCode MUSS 'OP' sein. Gefunden: "<value-of select="@contextControlCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:patientRole[not(@nullFlavor)][hl7:patient]) &gt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:patientRole[not(@nullFlavor)][hl7:patient] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:patientRole[not(@nullFlavor)][hl7:patient]) &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:patientRole[not(@nullFlavor)][hl7:patient] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]"
         id="d5104e152-false-d5314e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="string(@classCode) = ('PAT') or not(@classCode)">(atcdabbr_header_RecordTarget_eImpfpass): Der Wert von classCode MUSS 'PAT' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="not(hl7:id[1]/@nullFlavor)">(atcdabbr_header_RecordTarget_eImpfpass): Die Verwendung von id/@nullFlavor ist an dieser Stelle NICHT ERLAUBT.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="not(hl7:id[2]/@nullFlavor='UNK') or hl7:id[@root='1.2.40.0.10.2.1.1.149']">(atcdabbr_header_RecordTarget_eImpfpass): Wenn die SVNR mit nullFlavor 'UNK' angegeben wird, MUSS das bPK-GH strukturiert sein.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="not(hl7:id[2]/@nullFlavor) or (hl7:id[2][@nullFlavor='UNK'] or hl7:id[2][@nullFlavor='NI'])">(atcdabbr_header_RecordTarget_eImpfpass): Zugelassene nullFlavor sind "NI" und "UNK"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:id) &gt;= 2">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:id ist required [min 2x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:addr[not(@nullFlavor)]) &lt;= 2">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:addr[not(@nullFlavor)] kommt zu häufig vor [max 2x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:patient[not(@nullFlavor)]) &gt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:patient[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:patient[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:patient[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:id
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:id"
         id="d5104e159-false-d5391e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_header_RecordTarget_eImpfpass): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_AddressCompilation): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber)) or ((hl7:streetAddressLine or (hl7:streetName and hl7:houseNumber)) and not((hl7:streetAddressLine and hl7:streetName and hl7:houseNumber) or (hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber))))">(atcdabbr_other_AddressCompilation): Es muss entweder streetAddressLine oder streetName UND houseNumber angegeben
                werden.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetAddressLine) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetAddressLine kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetName) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetName kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:houseNumber) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:houseNumber kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:state) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:state kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:additionalLocator) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:additionalLocator kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]/hl7:streetName
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]/hl7:streetName">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]/hl7:houseNumber
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]/hl7:houseNumber">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]/hl7:state
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]/hl7:state">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="info"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="string-length(text()) = 3">(atcdabbr_other_AddressCompilation): Es wird EMPFOHLEN, den Staat im ISO 3 Ländercode anzugeben.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:telecom[not(@nullFlavor)]
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:telecom[not(@nullFlavor)]"
         id="d5104e322-false-d5553e0">
      <extends rule="TEL.AT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabbr_header_RecordTarget_eImpfpass): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="@value">(atcdabbr_header_RecordTarget_eImpfpass): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="not(@value) or string(@value castable as xs:anyURI)">(atcdabbr_header_RecordTarget_eImpfpass): Attribute @value MUSS vom Datentyp 'url' sein  - '<value-of select="@value"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_header_RecordTarget_eImpfpass): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]">
      <extends rule="d6933e0-false-d6937e0"/>
      <let name="elmcount"
           value="count(hl7:administrativeGenderCode[not(@nullFlavor)] | hl7:administrativeGenderCode[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="$elmcount &gt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Auswahl (hl7:administrativeGenderCode[not(@nullFlavor)]  oder  hl7:administrativeGenderCode[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="$elmcount &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Auswahl (hl7:administrativeGenderCode[not(@nullFlavor)]  oder  hl7:administrativeGenderCode[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:administrativeGenderCode[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:administrativeGenderCode[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:administrativeGenderCode[@nullFlavor='UNK']) &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:administrativeGenderCode[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:birthTime | hl7:birthTime[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="$elmcount &gt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Auswahl (hl7:birthTime  oder  hl7:birthTime[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="$elmcount &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Auswahl (hl7:birthTime  oder  hl7:birthTime[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:birthTime) &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:birthTime kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:birthTime[@nullFlavor='UNK']) &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:birthTime[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:maritalStatusCode) = 0">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:maritalStatusCode DARF NICHT vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:religiousAffiliationCode) = 0">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:religiousAffiliationCode DARF NICHT vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:raceCode) = 0">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:raceCode DARF NICHT vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:ethnicGroupCode) = 0">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:ethnicGroupCode DARF NICHT vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:birthplace[hl7:place]) &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:birthplace[hl7:place] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:languageCommunication) = 0">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:languageCommunication DARF NICHT vorkommen.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:administrativeGenderCode[not(@nullFlavor)]
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:administrativeGenderCode[not(@nullFlavor)]">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atcdabbr_header_RecordTarget_eImpfpass): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.10.4-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(atcdabbr_header_RecordTarget_eImpfpass): Der Elementinhalt MUSS einer von '1.2.40.0.34.10.4 ELGA_AdministrativeGender (DYNAMIC)' sein.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="@displayName">(atcdabbr_header_RecordTarget_eImpfpass): Attribut @displayName MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_header_RecordTarget_eImpfpass): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="@code">(atcdabbr_header_RecordTarget_eImpfpass): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atcdabbr_header_RecordTarget_eImpfpass): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="string(@codeSystem) = ('2.16.840.1.113883.5.1')">(atcdabbr_header_RecordTarget_eImpfpass): Der Wert von codeSystem MUSS '2.16.840.1.113883.5.1' sein. Gefunden: "<value-of select="@codeSystem"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="string(@codeSystemName) = ('HL7:AdministrativeGender') or not(@codeSystemName)">(atcdabbr_header_RecordTarget_eImpfpass): Der Wert von codeSystemName MUSS 'HL7:AdministrativeGender' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_header_RecordTarget_eImpfpass): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:administrativeGenderCode[@nullFlavor='UNK']
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:administrativeGenderCode[@nullFlavor='UNK']">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atcdabbr_header_RecordTarget_eImpfpass): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="string(@nullFlavor) = ('UNK')">(atcdabbr_header_RecordTarget_eImpfpass): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthTime
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthTime">
      <extends rule="TS.AT.VAR"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_header_RecordTarget_eImpfpass): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="not(*)">(atcdabbr_header_RecordTarget_eImpfpass): <value-of select="local-name()"/> with datatype TS.AT.VAR, SHOULD NOT have child elements.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthTime[@nullFlavor='UNK']
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthTime[@nullFlavor='UNK']">
      <extends rule="TS.AT.VAR"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_header_RecordTarget_eImpfpass): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="not(*)">(atcdabbr_header_RecordTarget_eImpfpass): <value-of select="local-name()"/> with datatype TS.AT.VAR, SHOULD NOT have child elements.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="string(@nullFlavor) = ('UNK')">(atcdabbr_header_RecordTarget_eImpfpass): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:maritalStatusCode
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:religiousAffiliationCode
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:raceCode
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:ethnicGroupCode
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="string(@classCode) = ('GUARD') or not(@classCode)">(atcdabbr_header_RecordTarget_eImpfpass): Der Wert von classCode MUSS 'GUARD' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:addr) &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:addr kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:guardianPerson | hl7:guardianPerson | hl7:guardianOrganization)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="$elmcount &gt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Auswahl (hl7:guardianPerson  oder  hl7:guardianPerson  oder  hl7:guardianOrganization) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="$elmcount &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Auswahl (hl7:guardianPerson  oder  hl7:guardianPerson  oder  hl7:guardianOrganization) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:guardianPerson) &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:guardianPerson kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:guardianPerson) &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:guardianPerson kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:guardianOrganization) &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:guardianOrganization kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_AddressCompilation): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber)) or ((hl7:streetAddressLine or (hl7:streetName and hl7:houseNumber)) and not((hl7:streetAddressLine and hl7:streetName and hl7:houseNumber) or (hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber))))">(atcdabbr_other_AddressCompilation): Es muss entweder streetAddressLine oder streetName UND houseNumber angegeben
                werden.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetAddressLine) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetAddressLine kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetName) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetName kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:houseNumber) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:houseNumber kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:state) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:state kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:additionalLocator) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:additionalLocator kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr/hl7:streetAddressLine
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr/hl7:streetAddressLine">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr/hl7:streetName
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr/hl7:streetName">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr/hl7:houseNumber
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr/hl7:houseNumber">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr/hl7:postalCode[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr/hl7:postalCode[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr/hl7:city[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr/hl7:city[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr/hl7:state
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr/hl7:state">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr/hl7:country[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr/hl7:country[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="info"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="string-length(text()) = 3">(atcdabbr_other_AddressCompilation): Es wird EMPFOHLEN, den Staat im ISO 3 Ländercode anzugeben.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr/hl7:additionalLocator
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:addr/hl7:additionalLocator">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:telecom
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:telecom">
      <extends rule="TEL.AT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabbr_header_RecordTarget_eImpfpass): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="@value">(atcdabbr_header_RecordTarget_eImpfpass): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="not(@value) or string-length(@value)&gt;0">(atcdabbr_header_RecordTarget_eImpfpass): Attribute @value MUSS vom Datentyp 'st' sein  - '<value-of select="@value"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="not(@use) or string-length(@use) &gt; 0">(atcdabbr_header_RecordTarget_eImpfpass): Attribute @use MUSS vom Datentyp 'set_cs' sein  - '<value-of select="@use"/>'</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.12
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson
Item: (atcdabbr_other_PersonNameCompilationG1M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.12-2023-04-17T091056.html"
              test="string(@classCode) = ('PSN') or not(@classCode)">(atcdabbr_other_PersonNameCompilationG1M): Der Wert von classCode MUSS 'PSN' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.12-2023-04-17T091056.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(atcdabbr_other_PersonNameCompilationG1M): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.12-2023-04-17T091056.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG1M): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.12-2023-04-17T091056.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_PersonNameCompilationG1M): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.12
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson/hl7:name[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG1M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson/hl7:name[not(@nullFlavor)]">
      <extends rule="PN"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.12-2023-04-17T091056.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PN')">(atcdabbr_other_PersonNameCompilationG1M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:PN" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.12-2023-04-17T091056.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_PersonNameCompilationG1M): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="string(@classCode) = ('PSN') or not(@classCode)">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von classCode MUSS 'PSN' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson/hl7:name[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson/hl7:name[not(@nullFlavor)]">
      <extends rule="PN"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PN')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:PN" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_PersonNameCompilationG2M): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:family[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:family[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:given[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:given[not(@nullFlavor)] ist mandatory [min 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson/hl7:name[not(@nullFlavor)]/hl7:prefix
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson/hl7:name[not(@nullFlavor)]/hl7:prefix">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson/hl7:name[not(@nullFlavor)]/hl7:family[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson/hl7:name[not(@nullFlavor)]/hl7:family[not(@nullFlavor)]">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson/hl7:name[not(@nullFlavor)]/hl7:given[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson/hl7:name[not(@nullFlavor)]/hl7:given[not(@nullFlavor)]">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson/hl7:name[not(@nullFlavor)]/hl7:suffix
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianPerson/hl7:name[not(@nullFlavor)]/hl7:suffix">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianOrganization
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.27
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianOrganization
Item: (atcdabbr_other_OrganizationNameCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianOrganization">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.27-2021-06-28T140014.html"
              test="string(@classCode) = ('ORG') or not(@classCode)">(atcdabbr_other_OrganizationNameCompilation): Der Wert von classCode MUSS 'ORG' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.27-2021-06-28T140014.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(atcdabbr_other_OrganizationNameCompilation): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.27-2021-06-28T140014.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_OrganizationNameCompilation): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.27-2021-06-28T140014.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_OrganizationNameCompilation): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.27
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianOrganization/hl7:name[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationNameCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:guardian/hl7:guardianOrganization/hl7:name[not(@nullFlavor)]">
      <extends rule="ON"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.27-2021-06-28T140014.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ON')">(atcdabbr_other_OrganizationNameCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ON" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="string(@classCode) = ('BIRTHPL') or not(@classCode)">(atcdabbr_header_RecordTarget_eImpfpass): Der Wert von classCode MUSS 'BIRTHPL' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:place) &gt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:place ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:place) &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:place kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="string(@classCode) = ('PLC') or not(@classCode)">(atcdabbr_header_RecordTarget_eImpfpass): Der Wert von classCode MUSS 'PLC' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(atcdabbr_header_RecordTarget_eImpfpass): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <let name="elmcount" value="count(hl7:addr | hl7:addr)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="$elmcount &gt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Auswahl (hl7:addr  oder  hl7:addr) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="$elmcount &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Auswahl (hl7:addr  oder  hl7:addr) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:addr) &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:addr kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.31-2023-04-03T104229.html"
              test="count(hl7:addr) &lt;= 1">(atcdabbr_header_RecordTarget_eImpfpass): Element hl7:addr kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.10
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr
Item: (atcdabbr_other_AddressCompilationMinimal)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_AddressCompilationMinimal): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="not(hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber)) or ((hl7:streetAddressLine or (hl7:streetName and hl7:houseNumber)) and not((hl7:streetAddressLine and hl7:streetName and hl7:houseNumber) or (hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber))))">(atcdabbr_other_AddressCompilationMinimal): Es muss entweder streetAddressLine oder streetName UND houseNumber angegeben
                werden.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="count(hl7:streetAddressLine) &lt;= 1">(atcdabbr_other_AddressCompilationMinimal): Element hl7:streetAddressLine kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="count(hl7:streetName) &lt;= 1">(atcdabbr_other_AddressCompilationMinimal): Element hl7:streetName kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="count(hl7:houseNumber) &lt;= 1">(atcdabbr_other_AddressCompilationMinimal): Element hl7:houseNumber kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="count(hl7:postalCode) &lt;= 1">(atcdabbr_other_AddressCompilationMinimal): Element hl7:postalCode kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="count(hl7:city) &lt;= 1">(atcdabbr_other_AddressCompilationMinimal): Element hl7:city kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="count(hl7:state) &lt;= 1">(atcdabbr_other_AddressCompilationMinimal): Element hl7:state kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="count(hl7:country) &lt;= 1">(atcdabbr_other_AddressCompilationMinimal): Element hl7:country kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="count(hl7:additionalLocator) &lt;= 1">(atcdabbr_other_AddressCompilationMinimal): Element hl7:additionalLocator kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.10
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (atcdabbr_other_AddressCompilationMinimal)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:streetAddressLine">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.10
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:streetName
Item: (atcdabbr_other_AddressCompilationMinimal)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:streetName">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.10
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:houseNumber
Item: (atcdabbr_other_AddressCompilationMinimal)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:houseNumber">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.10
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:postalCode
Item: (atcdabbr_other_AddressCompilationMinimal)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:postalCode">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.10
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:city
Item: (atcdabbr_other_AddressCompilationMinimal)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:city">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.10
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:state
Item: (atcdabbr_other_AddressCompilationMinimal)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:state">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.10
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:country
Item: (atcdabbr_other_AddressCompilationMinimal)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:country">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="info"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="string-length(text()) = 3">(atcdabbr_other_AddressCompilationMinimal): content length = 3 characters</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.10
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:additionalLocator
Item: (atcdabbr_other_AddressCompilationMinimal)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:additionalLocator">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.10-2023-04-06T143134.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_AddressCompilation): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber)) or ((hl7:streetAddressLine or (hl7:streetName and hl7:houseNumber)) and not((hl7:streetAddressLine and hl7:streetName and hl7:houseNumber) or (hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber))))">(atcdabbr_other_AddressCompilation): Es muss entweder streetAddressLine oder streetName UND houseNumber angegeben
                werden.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetAddressLine) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetAddressLine kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetName) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetName kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:houseNumber) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:houseNumber kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:state) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:state kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:additionalLocator) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:additionalLocator kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:streetAddressLine
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:streetAddressLine">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:streetName
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:streetName">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:houseNumber
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:houseNumber">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:postalCode[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:postalCode[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:city[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:city[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:state
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:state">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:country[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:country[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="info"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="string-length(text()) = 3">(atcdabbr_other_AddressCompilation): Es wird EMPFOHLEN, den Staat im ISO 3 Ländercode anzugeben.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:additionalLocator
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:birthplace[hl7:place]/hl7:place/hl7:addr/hl7:additionalLocator">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.31
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:languageCommunication
Item: (atcdabbr_header_RecordTarget_eImpfpass)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1" id="d6933e0-false-d6937e0" abstract="true">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="string(@classCode) = ('PSN') or not(@classCode)">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von classCode MUSS 'PSN' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]">
      <extends rule="PN"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PN')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:PN" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_PersonNameCompilationG2M): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:family[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:family[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:given[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:given[not(@nullFlavor)] ist mandatory [min 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:prefix
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:prefix">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:family[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:family[not(@nullFlavor)]">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:given[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:given[not(@nullFlavor)]">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:suffix
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:recordTarget[not(@nullFlavor)][hl7:patientRole]/hl7:patientRole[not(@nullFlavor)][hl7:patient]/hl7:patient[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:suffix">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]
Item: (atcdabbr_header_Author)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]"
         id="d5620e1001-false-d7111e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="string(@typeCode) = ('AUT') or not(@typeCode)">(atcdabbr_header_Author): Der Wert von typeCode MUSS 'AUT' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="string(@contextControlCode) = ('OP') or not(@contextControlCode)">(atcdabbr_header_Author): Der Wert von contextControlCode MUSS 'OP' sein. Gefunden: "<value-of select="@contextControlCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="count(hl7:functionCode) &lt;= 1">(atcdabbr_header_Author): Element hl7:functionCode kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="$elmcount &gt;= 1">(atcdabbr_header_Author): Auswahl (hl7:time[not(@nullFlavor)]  oder  hl7:time[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="$elmcount &lt;= 1">(atcdabbr_header_Author): Auswahl (hl7:time[not(@nullFlavor)]  oder  hl7:time[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="count(hl7:time[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_Author): Element hl7:time[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="count(hl7:time[@nullFlavor='UNK']) &lt;= 1">(atcdabbr_header_Author): Element hl7:time[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="count(hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]) &gt;= 1">(atcdabbr_header_Author): Element hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="count(hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]) &lt;= 1">(atcdabbr_header_Author): Element hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:functionCode
Item: (atcdabbr_header_Author)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:functionCode"
         id="d5620e1012-false-d7181e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atcdabbr_header_Author): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="@code">(atcdabbr_header_Author): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atcdabbr_header_Author): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="@codeSystem">(atcdabbr_header_Author): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(atcdabbr_header_Author): Attribute @codeSystem MUSS vom Datentyp 'oid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="@displayName">(atcdabbr_header_Author): Attribut @displayName MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_header_Author): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:time[not(@nullFlavor)]
Item: (atcdabbr_header_Author)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:time[not(@nullFlavor)]"
         id="d5620e1040-false-d7205e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_header_Author): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="not(*)">(atcdabbr_header_Author): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:time[@nullFlavor='UNK']
Item: (atcdabbr_header_Author)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:time[@nullFlavor='UNK']"
         id="d5620e1042-false-d7216e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_header_Author): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="not(*)">(atcdabbr_header_Author): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="string(@nullFlavor) = ('UNK')">(atcdabbr_header_Author): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]
Item: (atcdabbr_header_Author)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]"
         id="d5620e1049-false-d7257e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="string(@classCode) = ('ASSIGNED') or not(@classCode)">(atcdabbr_header_Author): Der Wert von classCode MUSS 'ASSIGNED' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <let name="elmcount"
           value="count(hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="$elmcount &gt;= 1">(atcdabbr_header_Author): Auswahl (hl7:id[not(@nullFlavor)]  oder  hl7:id[@nullFlavor='NI']  oder  hl7:id[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="count(hl7:id[@nullFlavor='NI']) &lt;= 1">(atcdabbr_header_Author): Element hl7:id[@nullFlavor='NI'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="count(hl7:id[@nullFlavor='UNK']) &lt;= 1">(atcdabbr_header_Author): Element hl7:id[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &lt;= 1">(atcdabbr_header_Author): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:assignedPerson | hl7:assignedAuthoringDevice)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="$elmcount &gt;= 1">(atcdabbr_header_Author): Auswahl (hl7:assignedPerson  oder  hl7:assignedAuthoringDevice) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="$elmcount &lt;= 1">(atcdabbr_header_Author): Auswahl (hl7:assignedPerson  oder  hl7:assignedAuthoringDevice) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="count(hl7:assignedPerson) &lt;= 1">(atcdabbr_header_Author): Element hl7:assignedPerson kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="count(hl7:assignedAuthoringDevice) &lt;= 1">(atcdabbr_header_Author): Element hl7:assignedAuthoringDevice kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="count(hl7:representedOrganization[not(@nullFlavor)]) &gt;= 1">(atcdabbr_header_Author): Element hl7:representedOrganization[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="count(hl7:representedOrganization[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_Author): Element hl7:representedOrganization[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:id[not(@nullFlavor)]
Item: (atcdabbr_header_Author)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:id[not(@nullFlavor)]"
         id="d5620e1079-false-d7356e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_header_Author): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:id[@nullFlavor='NI']
Item: (atcdabbr_header_Author)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:id[@nullFlavor='NI']"
         id="d5620e1091-false-d7364e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_header_Author): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="string(@nullFlavor) = ('NI')">(atcdabbr_header_Author): Der Wert von nullFlavor MUSS 'NI' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:id[@nullFlavor='UNK']
Item: (atcdabbr_header_Author)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:id[@nullFlavor='UNK']"
         id="d5620e1096-false-d7376e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_header_Author): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="string(@nullFlavor) = ('UNK')">(atcdabbr_header_Author): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]
Item: (atcdabbr_header_Author)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]"
         id="d5620e1102-false-d7393e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atcdabbr_header_Author): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(atcdabbr_header_Author): Der Elementinhalt MUSS einer von '1.2.40.0.34.10.6 ELGA_AuthorSpeciality (DYNAMIC)' sein.</assert>
      <report fpi="CD-DEPR-BSP"
              role="warning"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="exists(doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept[@type='D'][@code = $theCode][@codeSystem = $theCodeSystem])">(atcdabbr_header_Author): Element hl7:code ist codiert mit Bindungsstärke 'required' und enthält einen Code der veraltet ist.</report>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="@codeSystem">(atcdabbr_header_Author): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(atcdabbr_header_Author): Attribute @codeSystem MUSS vom Datentyp 'oid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="@displayName">(atcdabbr_header_Author): Attribut @displayName MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_header_Author): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="@code">(atcdabbr_header_Author): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atcdabbr_header_Author): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:telecom[not(@nullFlavor)]
Item: (atcdabbr_header_Author)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:telecom[not(@nullFlavor)]"
         id="d5620e1121-false-d7428e0">
      <extends rule="TEL.AT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabbr_header_Author): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="@value">(atcdabbr_header_Author): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="not(@value) or string-length(@value)&gt;0">(atcdabbr_header_Author): Attribute @value MUSS vom Datentyp 'st' sein  - '<value-of select="@value"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.2-2023-04-06T152319.html"
              test="not(@use) or string-length(@use) &gt; 0">(atcdabbr_header_Author): Attribute @use MUSS vom Datentyp 'set_cs' sein  - '<value-of select="@use"/>'</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedPerson
Item: (atcdabbr_header_Author)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedPerson
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedPerson">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="string(@classCode) = ('PSN') or not(@classCode)">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von classCode MUSS 'PSN' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]">
      <extends rule="PN"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PN')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:PN" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_PersonNameCompilationG2M): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:family[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:family[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:given[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:given[not(@nullFlavor)] ist mandatory [min 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/hl7:prefix
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/hl7:prefix">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/hl7:family[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/hl7:family[not(@nullFlavor)]">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/hl7:given[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/hl7:given[not(@nullFlavor)]">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/hl7:suffix
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/hl7:suffix">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedAuthoringDevice
Item: (atcdabbr_header_Author)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.18
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedAuthoringDevice
Item: (atcdabbr_other_DeviceCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedAuthoringDevice">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.18-2023-04-06T142415.html"
              test="string(@classCode) = ('DEV') or not(@classCode)">(atcdabbr_other_DeviceCompilation): Der Wert von classCode MUSS 'DEV' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.18-2023-04-06T142415.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(atcdabbr_other_DeviceCompilation): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.18-2023-04-06T142415.html"
              test="count(hl7:manufacturerModelName[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_DeviceCompilation): Element hl7:manufacturerModelName[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.18-2023-04-06T142415.html"
              test="count(hl7:manufacturerModelName[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_DeviceCompilation): Element hl7:manufacturerModelName[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.18-2023-04-06T142415.html"
              test="count(hl7:softwareName[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_DeviceCompilation): Element hl7:softwareName[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.18-2023-04-06T142415.html"
              test="count(hl7:softwareName[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_DeviceCompilation): Element hl7:softwareName[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.18
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedAuthoringDevice/hl7:manufacturerModelName[not(@nullFlavor)]
Item: (atcdabbr_other_DeviceCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedAuthoringDevice/hl7:manufacturerModelName[not(@nullFlavor)]">
      <extends rule="SC"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.18-2023-04-06T142415.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SC')">(atcdabbr_other_DeviceCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SC" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.18
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedAuthoringDevice/hl7:softwareName[not(@nullFlavor)]
Item: (atcdabbr_other_DeviceCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:assignedAuthoringDevice/hl7:softwareName[not(@nullFlavor)]">
      <extends rule="SC"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.18-2023-04-06T142415.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SC')">(atcdabbr_other_DeviceCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SC" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]
Item: (atcdabbr_header_Author)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.5
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithIdName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.5-2021-06-28T135753.html"
              test="string(@classCode) = ('ORG') or not(@classCode)">(atcdabbr_other_OrganizationCompilationWithIdName): Der Wert von classCode MUSS 'ORG' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.5-2021-06-28T135753.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(atcdabbr_other_OrganizationCompilationWithIdName): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.5-2021-06-28T135753.html"
              test="count(hl7:id[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_OrganizationCompilationWithIdName): Element hl7:id[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.5-2021-06-28T135753.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_OrganizationCompilationWithIdName): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.5-2021-06-28T135753.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_OrganizationCompilationWithIdName): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.5-2021-06-28T135753.html"
              test="count(hl7:addr[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_OrganizationCompilationWithIdName): Element hl7:addr[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.5
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithIdName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.5-2021-06-28T135753.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_other_OrganizationCompilationWithIdName): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.5
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithIdName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]">
      <extends rule="ON"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.5-2021-06-28T135753.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ON')">(atcdabbr_other_OrganizationCompilationWithIdName): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ON" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.5
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:telecom[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithIdName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:telecom[not(@nullFlavor)]">
      <extends rule="TEL.AT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.5-2021-06-28T135753.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabbr_other_OrganizationCompilationWithIdName): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.5-2021-06-28T135753.html"
              test="@value">(atcdabbr_other_OrganizationCompilationWithIdName): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.5-2021-06-28T135753.html"
              test="not(@value) or string-length(@value)&gt;0">(atcdabbr_other_OrganizationCompilationWithIdName): Attribute @value MUSS vom Datentyp 'st' sein  - '<value-of select="@value"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.5-2021-06-28T135753.html"
              test="not(@use) or string-length(@use) &gt; 0">(atcdabbr_other_OrganizationCompilationWithIdName): Attribute @use MUSS vom Datentyp 'set_cs' sein  - '<value-of select="@use"/>'</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.5
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithIdName)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_AddressCompilation): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber)) or ((hl7:streetAddressLine or (hl7:streetName and hl7:houseNumber)) and not((hl7:streetAddressLine and hl7:streetName and hl7:houseNumber) or (hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber))))">(atcdabbr_other_AddressCompilation): Es muss entweder streetAddressLine oder streetName UND houseNumber angegeben
                werden.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetAddressLine) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetAddressLine kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetName) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetName kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:houseNumber) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:houseNumber kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:state) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:state kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:additionalLocator) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:additionalLocator kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:streetName
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:streetName">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:houseNumber
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:houseNumber">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:state
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:state">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="info"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="string-length(text()) = 3">(atcdabbr_other_AddressCompilation): Es wird EMPFOHLEN, den Staat im ISO 3 Ländercode anzugeben.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[not(@nullFlavor)][hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]
Item: (atcdabbr_header_Data_Enterer)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]"
         id="d7705e240-false-d7979e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.22-2023-04-05T131903.html"
              test="string(@typeCode) = ('ENT') or not(@typeCode)">(atcdabbr_header_Data_Enterer): Der Wert von typeCode MUSS 'ENT' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.22-2023-04-05T131903.html"
              test="string(@contextControlCode) = ('OP') or not(@contextControlCode)">(atcdabbr_header_Data_Enterer): Der Wert von contextControlCode MUSS 'OP' sein. Gefunden: "<value-of select="@contextControlCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.22-2023-04-05T131903.html"
              test="count(hl7:time[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_Data_Enterer): Element hl7:time[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.22-2023-04-05T131903.html"
              test="count(hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]) &gt;= 1">(atcdabbr_header_Data_Enterer): Element hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.22-2023-04-05T131903.html"
              test="count(hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]) &lt;= 1">(atcdabbr_header_Data_Enterer): Element hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:time[not(@nullFlavor)]
Item: (atcdabbr_header_Data_Enterer)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:time[not(@nullFlavor)]"
         id="d7705e252-false-d8043e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.22-2023-04-05T131903.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_header_Data_Enterer): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.22-2023-04-05T131903.html"
              test="not(*)">(atcdabbr_header_Data_Enterer): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]
Item: (atcdabbr_header_Data_Enterer)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="string(@classCode) = ('ASSIGNED') or not(@classCode)">( atcdabbr_other_AssignedEntity): Der Wert von classCode MUSS 'ASSIGNED' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <let name="elmcount"
           value="count(hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="$elmcount &gt;= 1">( atcdabbr_other_AssignedEntity): Auswahl (hl7:id[not(@nullFlavor)]  oder  hl7:id[@nullFlavor='NI']  oder  hl7:id[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:id[@nullFlavor='NI']) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:id[@nullFlavor='NI'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:id[@nullFlavor='UNK']) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:id[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:addr[not(@nullFlavor)] | hl7:addr[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="$elmcount &lt;= 1">( atcdabbr_other_AssignedEntity): Auswahl (hl7:addr[not(@nullFlavor)]  oder  hl7:addr[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:addr[not(@nullFlavor)]) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:addr[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:addr[@nullFlavor='UNK']) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:addr[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:assignedPerson[not(@nullFlavor)]) &gt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:assignedPerson[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:assignedPerson[not(@nullFlavor)]) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:assignedPerson[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:representedOrganization) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:representedOrganization kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[not(@nullFlavor)]
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[not(@nullFlavor)]">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">( atcdabbr_other_AssignedEntity): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[@nullFlavor='NI']
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[@nullFlavor='NI']">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">( atcdabbr_other_AssignedEntity): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="string(@nullFlavor) = ('NI')">( atcdabbr_other_AssignedEntity): Der Wert von nullFlavor MUSS 'NI' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[@nullFlavor='UNK']
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[@nullFlavor='UNK']">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">( atcdabbr_other_AssignedEntity): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="string(@nullFlavor) = ('UNK')">( atcdabbr_other_AssignedEntity): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]
Item: ( atcdabbr_other_AssignedEntity)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_AddressCompilation): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber)) or ((hl7:streetAddressLine or (hl7:streetName and hl7:houseNumber)) and not((hl7:streetAddressLine and hl7:streetName and hl7:houseNumber) or (hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber))))">(atcdabbr_other_AddressCompilation): Es muss entweder streetAddressLine oder streetName UND houseNumber angegeben
                werden.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetAddressLine) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetAddressLine kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetName) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetName kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:houseNumber) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:houseNumber kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:state) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:state kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:additionalLocator) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:additionalLocator kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:streetName
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:streetName">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:houseNumber
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:houseNumber">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:state
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:state">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="info"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="string-length(text()) = 3">(atcdabbr_other_AddressCompilation): Es wird EMPFOHLEN, den Staat im ISO 3 Ländercode anzugeben.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[@nullFlavor='UNK']
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[@nullFlavor='UNK']">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="string(@nullFlavor) = ('UNK')">( atcdabbr_other_AssignedEntity): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:telecom[not(@nullFlavor)]
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:telecom[not(@nullFlavor)]">
      <extends rule="TEL.AT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">( atcdabbr_other_AssignedEntity): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="@value">( atcdabbr_other_AssignedEntity): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="not(@value) or string(@value castable as xs:anyURI)">( atcdabbr_other_AssignedEntity): Attribute @value MUSS vom Datentyp 'url' sein  - '<value-of select="@value"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">( atcdabbr_other_AssignedEntity): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]
Item: ( atcdabbr_other_AssignedEntity)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="string(@classCode) = ('PSN') or not(@classCode)">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von classCode MUSS 'PSN' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]">
      <extends rule="PN"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PN')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:PN" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_PersonNameCompilationG2M): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:family[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:family[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:given[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:given[not(@nullFlavor)] ist mandatory [min 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:prefix
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:prefix">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:family[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:family[not(@nullFlavor)]">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:given[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:given[not(@nullFlavor)]">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:suffix
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:suffix">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization
Item: ( atcdabbr_other_AssignedEntity)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="string(@classCode) = ('ORG') or not(@classCode)">(atcdabbr_other_OrganizationCompilationWithName): Der Wert von classCode MUSS 'ORG' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(atcdabbr_other_OrganizationCompilationWithName): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_OrganizationCompilationWithName): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_OrganizationCompilationWithName): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="count(hl7:addr[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_OrganizationCompilationWithName): Element hl7:addr[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:id[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:id[not(@nullFlavor)]">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_other_OrganizationCompilationWithName): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:name[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:name[not(@nullFlavor)]">
      <extends rule="ON"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ON')">(atcdabbr_other_OrganizationCompilationWithName): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ON" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:telecom[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:telecom[not(@nullFlavor)]">
      <extends rule="TEL.AT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabbr_other_OrganizationCompilationWithName): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="@value">(atcdabbr_other_OrganizationCompilationWithName): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="not(@value) or string-length(@value)&gt;0">(atcdabbr_other_OrganizationCompilationWithName): Attribute @value MUSS vom Datentyp 'st' sein  - '<value-of select="@value"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="not(@use) or string-length(@use) &gt; 0">(atcdabbr_other_OrganizationCompilationWithName): Attribute @use MUSS vom Datentyp 'set_cs' sein  - '<value-of select="@use"/>'</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_AddressCompilation): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber)) or ((hl7:streetAddressLine or (hl7:streetName and hl7:houseNumber)) and not((hl7:streetAddressLine and hl7:streetName and hl7:houseNumber) or (hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber))))">(atcdabbr_other_AddressCompilation): Es muss entweder streetAddressLine oder streetName UND houseNumber angegeben
                werden.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetAddressLine) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetAddressLine kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetName) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetName kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:houseNumber) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:houseNumber kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:state) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:state kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:additionalLocator) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:additionalLocator kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:streetName
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:streetName">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:houseNumber
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:houseNumber">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:state
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:state">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="info"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="string-length(text()) = 3">(atcdabbr_other_AddressCompilation): Es wird EMPFOHLEN, den Staat im ISO 3 Ländercode anzugeben.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:dataEnterer[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.4
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]
Item: (atcdabbr_header_Custodian)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]"
         id="d8097e580-false-d8833e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="string(@typeCode) = ('CST') or not(@typeCode)">(atcdabbr_header_Custodian): Der Wert von typeCode MUSS 'CST' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="count(hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]) &gt;= 1">(atcdabbr_header_Custodian): Element hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="count(hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]) &lt;= 1">(atcdabbr_header_Custodian): Element hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.4
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]
Item: (atcdabbr_header_Custodian)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]"
         id="d8097e587-false-d8875e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="string(@classCode) = ('ASSIGNED') or not(@classCode)">(atcdabbr_header_Custodian): Der Wert von classCode MUSS 'ASSIGNED' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="count(hl7:representedCustodianOrganization[not(@nullFlavor)]) &gt;= 1">(atcdabbr_header_Custodian): Element hl7:representedCustodianOrganization[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="count(hl7:representedCustodianOrganization[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_Custodian): Element hl7:representedCustodianOrganization[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.4
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]
Item: (atcdabbr_header_Custodian)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]"
         id="d8097e591-false-d8917e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="string(@classCode) = ('ORG') or not(@classCode)">(atcdabbr_header_Custodian): Der Wert von classCode MUSS 'ORG' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(atcdabbr_header_Custodian): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="count(hl7:id[not(@nullFlavor)]) &gt;= 1">(atcdabbr_header_Custodian): Element hl7:id[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(atcdabbr_header_Custodian): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_Custodian): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="count(hl7:addr[not(@nullFlavor)]) &gt;= 1">(atcdabbr_header_Custodian): Element hl7:addr[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="count(hl7:addr[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_Custodian): Element hl7:addr[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.4
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]
Item: (atcdabbr_header_Custodian)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]"
         id="d8097e597-false-d8968e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_header_Custodian): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.4
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]
Item: (atcdabbr_header_Custodian)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]"
         id="d8097e605-false-d8978e0">
      <extends rule="ON"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ON')">(atcdabbr_header_Custodian): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ON" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.4
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:telecom[not(@nullFlavor)]
Item: (atcdabbr_header_Custodian)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:telecom[not(@nullFlavor)]"
         id="d8097e611-false-d8986e0">
      <extends rule="TEL.AT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabbr_header_Custodian): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="@value">(atcdabbr_header_Custodian): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="not(@value) or string-length(@value)&gt;0">(atcdabbr_header_Custodian): Attribute @value MUSS vom Datentyp 'st' sein  - '<value-of select="@value"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.4-2021-10-13T140515.html"
              test="not(@use) or string-length(@use) &gt; 0">(atcdabbr_header_Custodian): Attribute @use MUSS vom Datentyp 'set_cs' sein  - '<value-of select="@use"/>'</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.4
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_header_Custodian)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_AddressCompilation): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber)) or ((hl7:streetAddressLine or (hl7:streetName and hl7:houseNumber)) and not((hl7:streetAddressLine and hl7:streetName and hl7:houseNumber) or (hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber))))">(atcdabbr_other_AddressCompilation): Es muss entweder streetAddressLine oder streetName UND houseNumber angegeben
                werden.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetAddressLine) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetAddressLine kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetName) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetName kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:houseNumber) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:houseNumber kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:state) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:state kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:additionalLocator) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:additionalLocator kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:streetName
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:streetName">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:houseNumber
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:houseNumber">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:state
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:state">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="info"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="string-length(text()) = 3">(atcdabbr_other_AddressCompilation): Es wird EMPFOHLEN, den Staat im ISO 3 Ländercode anzugeben.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:custodian[hl7:assignedCustodian]/hl7:assignedCustodian[not(@nullFlavor)][hl7:representedCustodianOrganization]/hl7:representedCustodianOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:informationRecipient
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.5
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]
Item: (atcdabbr_header_LegalAuthenticator)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]"
         id="d9174e80-false-d9220e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="string(@contextControlCode) = ('OP') or not(@contextControlCode)">(atcdabbr_header_LegalAuthenticator): Der Wert von contextControlCode MUSS 'OP' sein. Gefunden: "<value-of select="@contextControlCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="string(@typeCode) = ('LA') or not(@typeCode)">(atcdabbr_header_LegalAuthenticator): Der Wert von typeCode MUSS 'LA' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <let name="elmcount"
           value="count(hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="$elmcount &gt;= 1">(atcdabbr_header_LegalAuthenticator): Auswahl (hl7:time[not(@nullFlavor)]  oder  hl7:time[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="$elmcount &lt;= 1">(atcdabbr_header_LegalAuthenticator): Auswahl (hl7:time[not(@nullFlavor)]  oder  hl7:time[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="count(hl7:time[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_LegalAuthenticator): Element hl7:time[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="count(hl7:time[@nullFlavor='UNK']) &lt;= 1">(atcdabbr_header_LegalAuthenticator): Element hl7:time[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="count(hl7:signatureCode[@code = 'S']) &gt;= 1">(atcdabbr_header_LegalAuthenticator): Element hl7:signatureCode[@code = 'S'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="count(hl7:signatureCode[@code = 'S']) &lt;= 1">(atcdabbr_header_LegalAuthenticator): Element hl7:signatureCode[@code = 'S'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="count(hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]) &gt;= 1">(atcdabbr_header_LegalAuthenticator): Element hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="count(hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]) &lt;= 1">(atcdabbr_header_LegalAuthenticator): Element hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.5
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:time[not(@nullFlavor)]
Item: (atcdabbr_header_LegalAuthenticator)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:time[not(@nullFlavor)]"
         id="d9174e96-false-d9305e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_header_LegalAuthenticator): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="not(*)">(atcdabbr_header_LegalAuthenticator): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.5
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:time[@nullFlavor='UNK']
Item: (atcdabbr_header_LegalAuthenticator)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:time[@nullFlavor='UNK']"
         id="d9174e98-false-d9316e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_header_LegalAuthenticator): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="not(*)">(atcdabbr_header_LegalAuthenticator): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="string(@nullFlavor) = ('UNK')">(atcdabbr_header_LegalAuthenticator): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.5
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:signatureCode[@code = 'S']
Item: (atcdabbr_header_LegalAuthenticator)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:signatureCode[@code = 'S']"
         id="d9174e104-false-d9334e0">
      <extends rule="CS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CS')">(atcdabbr_header_LegalAuthenticator): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.5-2021-02-19T111059.html"
              test="@nullFlavor or (@code='S')">(atcdabbr_header_LegalAuthenticator): Der Elementinhalt MUSS einer von 'code 'S'' sein.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.5
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]
Item: (atcdabbr_header_LegalAuthenticator)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="string(@classCode) = ('ASSIGNED') or not(@classCode)">( atcdabbr_other_AssignedEntity): Der Wert von classCode MUSS 'ASSIGNED' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <let name="elmcount"
           value="count(hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="$elmcount &gt;= 1">( atcdabbr_other_AssignedEntity): Auswahl (hl7:id[not(@nullFlavor)]  oder  hl7:id[@nullFlavor='NI']  oder  hl7:id[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:id[@nullFlavor='NI']) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:id[@nullFlavor='NI'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:id[@nullFlavor='UNK']) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:id[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:addr[not(@nullFlavor)] | hl7:addr[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="$elmcount &lt;= 1">( atcdabbr_other_AssignedEntity): Auswahl (hl7:addr[not(@nullFlavor)]  oder  hl7:addr[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:addr[not(@nullFlavor)]) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:addr[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:addr[@nullFlavor='UNK']) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:addr[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:assignedPerson[not(@nullFlavor)]) &gt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:assignedPerson[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:assignedPerson[not(@nullFlavor)]) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:assignedPerson[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:representedOrganization) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:representedOrganization kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[not(@nullFlavor)]
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[not(@nullFlavor)]">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">( atcdabbr_other_AssignedEntity): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[@nullFlavor='NI']
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[@nullFlavor='NI']">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">( atcdabbr_other_AssignedEntity): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="string(@nullFlavor) = ('NI')">( atcdabbr_other_AssignedEntity): Der Wert von nullFlavor MUSS 'NI' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[@nullFlavor='UNK']
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[@nullFlavor='UNK']">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">( atcdabbr_other_AssignedEntity): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="string(@nullFlavor) = ('UNK')">( atcdabbr_other_AssignedEntity): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]
Item: ( atcdabbr_other_AssignedEntity)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_AddressCompilation): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber)) or ((hl7:streetAddressLine or (hl7:streetName and hl7:houseNumber)) and not((hl7:streetAddressLine and hl7:streetName and hl7:houseNumber) or (hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber))))">(atcdabbr_other_AddressCompilation): Es muss entweder streetAddressLine oder streetName UND houseNumber angegeben
                werden.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetAddressLine) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetAddressLine kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetName) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetName kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:houseNumber) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:houseNumber kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:state) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:state kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:additionalLocator) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:additionalLocator kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:streetName
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:streetName">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:houseNumber
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:houseNumber">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:state
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:state">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="info"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="string-length(text()) = 3">(atcdabbr_other_AddressCompilation): Es wird EMPFOHLEN, den Staat im ISO 3 Ländercode anzugeben.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[@nullFlavor='UNK']
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[@nullFlavor='UNK']">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="string(@nullFlavor) = ('UNK')">( atcdabbr_other_AssignedEntity): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:telecom[not(@nullFlavor)]
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:telecom[not(@nullFlavor)]">
      <extends rule="TEL.AT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">( atcdabbr_other_AssignedEntity): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="@value">( atcdabbr_other_AssignedEntity): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="not(@value) or string(@value castable as xs:anyURI)">( atcdabbr_other_AssignedEntity): Attribute @value MUSS vom Datentyp 'url' sein  - '<value-of select="@value"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">( atcdabbr_other_AssignedEntity): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]
Item: ( atcdabbr_other_AssignedEntity)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="string(@classCode) = ('PSN') or not(@classCode)">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von classCode MUSS 'PSN' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]">
      <extends rule="PN"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PN')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:PN" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_PersonNameCompilationG2M): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:family[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:family[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:given[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:given[not(@nullFlavor)] ist mandatory [min 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:prefix
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:prefix">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:family[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:family[not(@nullFlavor)]">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:given[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:given[not(@nullFlavor)]">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:suffix
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:suffix">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization
Item: ( atcdabbr_other_AssignedEntity)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="string(@classCode) = ('ORG') or not(@classCode)">(atcdabbr_other_OrganizationCompilationWithName): Der Wert von classCode MUSS 'ORG' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(atcdabbr_other_OrganizationCompilationWithName): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_OrganizationCompilationWithName): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_OrganizationCompilationWithName): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="count(hl7:addr[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_OrganizationCompilationWithName): Element hl7:addr[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:id[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:id[not(@nullFlavor)]">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_other_OrganizationCompilationWithName): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:name[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:name[not(@nullFlavor)]">
      <extends rule="ON"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ON')">(atcdabbr_other_OrganizationCompilationWithName): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ON" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:telecom[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:telecom[not(@nullFlavor)]">
      <extends rule="TEL.AT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabbr_other_OrganizationCompilationWithName): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="@value">(atcdabbr_other_OrganizationCompilationWithName): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="not(@value) or string-length(@value)&gt;0">(atcdabbr_other_OrganizationCompilationWithName): Attribute @value MUSS vom Datentyp 'st' sein  - '<value-of select="@value"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="not(@use) or string-length(@use) &gt; 0">(atcdabbr_other_OrganizationCompilationWithName): Attribute @use MUSS vom Datentyp 'set_cs' sein  - '<value-of select="@use"/>'</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_AddressCompilation): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber)) or ((hl7:streetAddressLine or (hl7:streetName and hl7:houseNumber)) and not((hl7:streetAddressLine and hl7:streetName and hl7:houseNumber) or (hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber))))">(atcdabbr_other_AddressCompilation): Es muss entweder streetAddressLine oder streetName UND houseNumber angegeben
                werden.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetAddressLine) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetAddressLine kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetName) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetName kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:houseNumber) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:houseNumber kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:state) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:state kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:additionalLocator) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:additionalLocator kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:streetName
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:streetName">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:houseNumber
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:houseNumber">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:state
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:state">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="info"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="string-length(text()) = 3">(atcdabbr_other_AddressCompilation): Es wird EMPFOHLEN, den Staat im ISO 3 Ländercode anzugeben.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:legalAuthenticator[hl7:signatureCode[@code = 'S']]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:authenticator
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:participant
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:inFulfillmentOf
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.32
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]
Item: (atcdabbr_header_DocumentationOfServiceEvent_eImpfpass)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]"
         id="d10133e108-false-d10141e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="string(@typeCode) = ('DOC') or not(@typeCode)">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Der Wert von typeCode MUSS 'DOC' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="count(hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]) &gt;= 1">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Element hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="count(hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]) &lt;= 1">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Element hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.32
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]/hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]
Item: (atcdabbr_header_DocumentationOfServiceEvent_eImpfpass)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]/hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]"
         id="d10133e115-false-d10163e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="string(@classCode) = ('ACT')">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Der Wert von classCode MUSS 'ACT' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="string(@moodCode) = ('EVN') or not(@moodCode)">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="count(hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]) &gt;= 1">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Element hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="count(hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]) &lt;= 1">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Element hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="count(hl7:effectiveTime[not(@nullFlavor)]) &gt;= 1">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Element hl7:effectiveTime[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="count(hl7:effectiveTime[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Element hl7:effectiveTime[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="count(hl7:performer) = 0">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Element hl7:performer DARF NICHT vorkommen.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.32
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]/hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]/hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]
Item: (atcdabbr_header_DocumentationOfServiceEvent_eImpfpass)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]/hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]/hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]"
         id="d10133e124-false-d10203e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="string(@code) = ('41000179103')">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Der Wert von code MUSS '41000179103' sein. Gefunden: "<value-of select="@code"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="string(@codeSystem) = ('2.16.840.1.113883.6.96')">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Der Wert von codeSystem MUSS '2.16.840.1.113883.6.96' sein. Gefunden: "<value-of select="@codeSystem"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="string(@codeSystemName) = ('SNOMED CT')">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Der Wert von codeSystemName MUSS 'SNOMED CT' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="string(@displayName) = ('Immunization record (record artifact)')">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Der Wert von displayName MUSS 'Immunization record (record artifact)' sein. Gefunden: "<value-of select="@displayName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.32
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]/hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]/hl7:effectiveTime[not(@nullFlavor)]
Item: (atcdabbr_header_DocumentationOfServiceEvent_eImpfpass)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]/hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]/hl7:effectiveTime[not(@nullFlavor)]"
         id="d10133e146-false-d10234e0">
      <extends rule="IVL_TS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVL_TS')">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVL_TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="elmcount"
           value="count(hl7:low[not(@nullFlavor)] | hl7:low[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="$elmcount &gt;= 1">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Auswahl (hl7:low[not(@nullFlavor)]  oder  hl7:low[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="$elmcount &lt;= 1">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Auswahl (hl7:low[not(@nullFlavor)]  oder  hl7:low[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="count(hl7:low[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Element hl7:low[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="count(hl7:low[@nullFlavor='UNK']) &lt;= 1">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Element hl7:low[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:high[not(@nullFlavor)] | hl7:high[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="$elmcount &gt;= 1">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Auswahl (hl7:high[not(@nullFlavor)]  oder  hl7:high[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="$elmcount &lt;= 1">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Auswahl (hl7:high[not(@nullFlavor)]  oder  hl7:high[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="count(hl7:high[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Element hl7:high[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="count(hl7:high[@nullFlavor='UNK']) &lt;= 1">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Element hl7:high[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.32
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]/hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]/hl7:effectiveTime[not(@nullFlavor)]/hl7:low[not(@nullFlavor)]
Item: (atcdabbr_header_DocumentationOfServiceEvent_eImpfpass)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]/hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]/hl7:effectiveTime[not(@nullFlavor)]/hl7:low[not(@nullFlavor)]"
         id="d10133e193-false-d10275e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="not(*)">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.32
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]/hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]/hl7:effectiveTime[not(@nullFlavor)]/hl7:low[@nullFlavor='UNK']
Item: (atcdabbr_header_DocumentationOfServiceEvent_eImpfpass)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]/hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]/hl7:effectiveTime[not(@nullFlavor)]/hl7:low[@nullFlavor='UNK']"
         id="d10133e226-false-d10286e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="not(*)">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="string(@nullFlavor) = ('UNK')">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.32
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]/hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]/hl7:effectiveTime[not(@nullFlavor)]/hl7:high[not(@nullFlavor)]
Item: (atcdabbr_header_DocumentationOfServiceEvent_eImpfpass)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]/hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]/hl7:effectiveTime[not(@nullFlavor)]/hl7:high[not(@nullFlavor)]"
         id="d10133e237-false-d10301e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="not(*)">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.32
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]/hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]/hl7:effectiveTime[not(@nullFlavor)]/hl7:high[@nullFlavor='UNK']
Item: (atcdabbr_header_DocumentationOfServiceEvent_eImpfpass)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]/hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]/hl7:effectiveTime[not(@nullFlavor)]/hl7:high[@nullFlavor='UNK']"
         id="d10133e270-false-d10312e0">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="not(*)">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.32-2023-04-05T132907.html"
              test="string(@nullFlavor) = ('UNK')">(atcdabbr_header_DocumentationOfServiceEvent_eImpfpass): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.32
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:documentationOf[hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]]/hl7:serviceEvent[hl7:code[(@code = '41000179103' and @codeSystem = '2.16.840.1.113883.6.96')]]/hl7:performer
Item: (atcdabbr_header_DocumentationOfServiceEvent_eImpfpass)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.14
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:relatedDocument[@typeCode][hl7:parentDocument]
Item: (atcdabbr_header_DocumentReplacementRelatedDocument)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:relatedDocument[@typeCode][hl7:parentDocument]"
         id="d10331e32-false-d10338e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.14-2021-06-28T134215.html"
              test="@typeCode">(atcdabbr_header_DocumentReplacementRelatedDocument): Attribut @typeCode MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.14-2021-06-28T134215.html"
              test="not(@typeCode) or (string-length(@typeCode) &gt; 0 and not(matches(@typeCode,'\s')))">(atcdabbr_header_DocumentReplacementRelatedDocument): Attribute @typeCode MUSS vom Datentyp 'cs' sein  - '<value-of select="@typeCode"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.14-2021-06-28T134215.html"
              test="count(hl7:parentDocument[not(@nullFlavor)]) &gt;= 1">(atcdabbr_header_DocumentReplacementRelatedDocument): Element hl7:parentDocument[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.14-2021-06-28T134215.html"
              test="count(hl7:parentDocument[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_DocumentReplacementRelatedDocument): Element hl7:parentDocument[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.14
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:relatedDocument[@typeCode][hl7:parentDocument]/hl7:parentDocument[not(@nullFlavor)]
Item: (atcdabbr_header_DocumentReplacementRelatedDocument)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:relatedDocument[@typeCode][hl7:parentDocument]/hl7:parentDocument[not(@nullFlavor)]"
         id="d10331e72-false-d10359e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.14-2021-06-28T134215.html"
              test="string(@classCode) = ('DOCCLIN') or not(@classCode)">(atcdabbr_header_DocumentReplacementRelatedDocument): Der Wert von classCode MUSS 'DOCCLIN' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.14-2021-06-28T134215.html"
              test="string(@moodCode) = ('EVN') or not(@moodCode)">(atcdabbr_header_DocumentReplacementRelatedDocument): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.14-2021-06-28T134215.html"
              test="count(hl7:id[not(@nullFlavor)]) &gt;= 1">(atcdabbr_header_DocumentReplacementRelatedDocument): Element hl7:id[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.14-2021-06-28T134215.html"
              test="count(hl7:id[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_DocumentReplacementRelatedDocument): Element hl7:id[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.14
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:relatedDocument[@typeCode][hl7:parentDocument]/hl7:parentDocument[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]
Item: (atcdabbr_header_DocumentReplacementRelatedDocument)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:relatedDocument[@typeCode][hl7:parentDocument]/hl7:parentDocument[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]"
         id="d10331e82-false-d10383e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.14-2021-06-28T134215.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_header_DocumentReplacementRelatedDocument): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:authorization
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.50
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]
Item: (atcdabbr_header_ComponentOfEncompassingEncounterWithId)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]"
         id="d10395e357-false-d10466e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="count(hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]) &gt;= 1">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Element hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="count(hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]) &lt;= 1">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Element hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.50
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]
Item: (atcdabbr_header_ComponentOfEncompassingEncounterWithId)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]"
         id="d10395e366-false-d10614e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="string(@classCode) = ('ENC') or not(@classCode)">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Der Wert von classCode MUSS 'ENC' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="string(@moodCode) = ('EVN') or not(@moodCode)">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <let name="elmcount"
           value="count(hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="$elmcount &gt;= 1">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Auswahl (hl7:id[not(@nullFlavor)]  oder  hl7:id[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="$elmcount &lt;= 1">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Auswahl (hl7:id[not(@nullFlavor)]  oder  hl7:id[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="count(hl7:id[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Element hl7:id[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="count(hl7:id[@nullFlavor='UNK']) &lt;= 1">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Element hl7:id[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &gt;= 1">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &lt;= 1">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="count(hl7:effectiveTime[not(@nullFlavor)]) &gt;= 1">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Element hl7:effectiveTime[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="count(hl7:effectiveTime[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Element hl7:effectiveTime[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="count(hl7:responsibleParty[hl7:assignedEntity]) &lt;= 1">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Element hl7:responsibleParty[hl7:assignedEntity] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="count(hl7:location[not(@nullFlavor)][hl7:healthCareFacility]) &gt;= 1">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Element hl7:location[not(@nullFlavor)][hl7:healthCareFacility] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="count(hl7:location[not(@nullFlavor)][hl7:healthCareFacility]) &lt;= 1">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Element hl7:location[not(@nullFlavor)][hl7:healthCareFacility] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.50
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:id[not(@nullFlavor)]
Item: (atcdabbr_header_ComponentOfEncompassingEncounterWithId)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:id[not(@nullFlavor)]"
         id="d10395e379-false-d10742e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="@extension">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Attribut @extension MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="@root">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Attribut @root MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@root,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@root,'^[A-Za-z][A-Za-z\d\-]*$')">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Attribute @root MUSS vom Datentyp 'uid' sein  - '<value-of select="@root"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="not(@assigningAuthorityName) or string-length(@assigningAuthorityName)&gt;0">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Attribute @assigningAuthorityName MUSS vom Datentyp 'st' sein  - '<value-of select="@assigningAuthorityName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.50
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:id[@nullFlavor='UNK']
Item: (atcdabbr_header_ComponentOfEncompassingEncounterWithId)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:id[@nullFlavor='UNK']"
         id="d10395e407-false-d10764e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.50
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]
Item: (atcdabbr_header_ComponentOfEncompassingEncounterWithId)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]"
         id="d10395e410-false-d10777e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Der Elementinhalt MUSS einer von '1.2.40.0.34.10.5 ELGA_ActEncounterCode (DYNAMIC)' sein.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="@code">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="@codeSystem">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Attribute @codeSystem MUSS vom Datentyp 'oid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="string(@codeSystemName) = ('HL7:ActCode') or not(@codeSystemName)">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Der Wert von codeSystemName MUSS 'HL7:ActCode' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="@displayName">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Attribut @displayName MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.50
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:effectiveTime[not(@nullFlavor)]
Item: (atcdabbr_header_ComponentOfEncompassingEncounterWithId)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:effectiveTime[not(@nullFlavor)]
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:effectiveTime[not(@nullFlavor)]">
      <let name="elmcount"
           value="count(hl7:low[@value] | hl7:low[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="$elmcount &gt;= 1">(atcdabbr_other_TimeIntervalInformationMinimal): Auswahl (hl7:low[@value]  oder  hl7:low[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="$elmcount &lt;= 1">(atcdabbr_other_TimeIntervalInformationMinimal): Auswahl (hl7:low[@value]  oder  hl7:low[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="count(hl7:low[@value]) &lt;= 1">(atcdabbr_other_TimeIntervalInformationMinimal): Element hl7:low[@value] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="count(hl7:low[@nullFlavor='UNK']) &lt;= 1">(atcdabbr_other_TimeIntervalInformationMinimal): Element hl7:low[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:high[@value] | hl7:high[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="$elmcount &gt;= 1">(atcdabbr_other_TimeIntervalInformationMinimal): Auswahl (hl7:high[@value]  oder  hl7:high[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="$elmcount &lt;= 1">(atcdabbr_other_TimeIntervalInformationMinimal): Auswahl (hl7:high[@value]  oder  hl7:high[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="count(hl7:high[@value]) &lt;= 1">(atcdabbr_other_TimeIntervalInformationMinimal): Element hl7:high[@value] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="count(hl7:high[@nullFlavor='UNK']) &lt;= 1">(atcdabbr_other_TimeIntervalInformationMinimal): Element hl7:high[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:effectiveTime[not(@nullFlavor)]/hl7:low[@value]
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:effectiveTime[not(@nullFlavor)]/hl7:low[@value]">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_other_TimeIntervalInformationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="not(*)">(atcdabbr_other_TimeIntervalInformationMinimal): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:effectiveTime[not(@nullFlavor)]/hl7:low[@nullFlavor='UNK']
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:effectiveTime[not(@nullFlavor)]/hl7:low[@nullFlavor='UNK']">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_other_TimeIntervalInformationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="not(*)">(atcdabbr_other_TimeIntervalInformationMinimal): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="string(@nullFlavor) = ('UNK')">(atcdabbr_other_TimeIntervalInformationMinimal): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:effectiveTime[not(@nullFlavor)]/hl7:high[@value]
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:effectiveTime[not(@nullFlavor)]/hl7:high[@value]">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_other_TimeIntervalInformationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="not(*)">(atcdabbr_other_TimeIntervalInformationMinimal): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:effectiveTime[not(@nullFlavor)]/hl7:high[@nullFlavor='UNK']
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:effectiveTime[not(@nullFlavor)]/hl7:high[@nullFlavor='UNK']">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_other_TimeIntervalInformationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="not(*)">(atcdabbr_other_TimeIntervalInformationMinimal): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="string(@nullFlavor) = ('UNK')">(atcdabbr_other_TimeIntervalInformationMinimal): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.50
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]
Item: (atcdabbr_header_ComponentOfEncompassingEncounterWithId)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]"
         id="d10395e461-false-d10963e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="count(hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]) &gt;= 1">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Element hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.50-2023-02-28T103728.html"
              test="count(hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]) &lt;= 1">(atcdabbr_header_ComponentOfEncompassingEncounterWithId): Element hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.50
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]
Item: (atcdabbr_header_ComponentOfEncompassingEncounterWithId)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="string(@classCode) = ('ASSIGNED') or not(@classCode)">( atcdabbr_other_AssignedEntity): Der Wert von classCode MUSS 'ASSIGNED' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <let name="elmcount"
           value="count(hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="$elmcount &gt;= 1">( atcdabbr_other_AssignedEntity): Auswahl (hl7:id[not(@nullFlavor)]  oder  hl7:id[@nullFlavor='NI']  oder  hl7:id[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:id[@nullFlavor='NI']) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:id[@nullFlavor='NI'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:id[@nullFlavor='UNK']) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:id[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:addr[not(@nullFlavor)] | hl7:addr[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="$elmcount &lt;= 1">( atcdabbr_other_AssignedEntity): Auswahl (hl7:addr[not(@nullFlavor)]  oder  hl7:addr[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:addr[not(@nullFlavor)]) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:addr[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:addr[@nullFlavor='UNK']) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:addr[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:assignedPerson[not(@nullFlavor)]) &gt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:assignedPerson[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:assignedPerson[not(@nullFlavor)]) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:assignedPerson[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="count(hl7:representedOrganization) &lt;= 1">( atcdabbr_other_AssignedEntity): Element hl7:representedOrganization kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[not(@nullFlavor)]
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[not(@nullFlavor)]">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">( atcdabbr_other_AssignedEntity): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[@nullFlavor='NI']
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[@nullFlavor='NI']">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">( atcdabbr_other_AssignedEntity): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="string(@nullFlavor) = ('NI')">( atcdabbr_other_AssignedEntity): Der Wert von nullFlavor MUSS 'NI' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[@nullFlavor='UNK']
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:id[@nullFlavor='UNK']">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">( atcdabbr_other_AssignedEntity): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="string(@nullFlavor) = ('UNK')">( atcdabbr_other_AssignedEntity): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]
Item: ( atcdabbr_other_AssignedEntity)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_AddressCompilation): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber)) or ((hl7:streetAddressLine or (hl7:streetName and hl7:houseNumber)) and not((hl7:streetAddressLine and hl7:streetName and hl7:houseNumber) or (hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber))))">(atcdabbr_other_AddressCompilation): Es muss entweder streetAddressLine oder streetName UND houseNumber angegeben
                werden.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetAddressLine) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetAddressLine kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetName) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetName kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:houseNumber) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:houseNumber kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:state) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:state kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:additionalLocator) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:additionalLocator kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:streetName
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:streetName">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:houseNumber
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:houseNumber">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:state
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:state">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="info"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="string-length(text()) = 3">(atcdabbr_other_AddressCompilation): Es wird EMPFOHLEN, den Staat im ISO 3 Ländercode anzugeben.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[@nullFlavor='UNK']
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:addr[@nullFlavor='UNK']">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="string(@nullFlavor) = ('UNK')">( atcdabbr_other_AssignedEntity): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:telecom[not(@nullFlavor)]
Item: ( atcdabbr_other_AssignedEntity)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:telecom[not(@nullFlavor)]">
      <extends rule="TEL.AT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">( atcdabbr_other_AssignedEntity): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="@value">( atcdabbr_other_AssignedEntity): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="not(@value) or string(@value castable as xs:anyURI)">( atcdabbr_other_AssignedEntity): Attribute @value MUSS vom Datentyp 'url' sein  - '<value-of select="@value"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.22-2023-04-13T131455.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">( atcdabbr_other_AssignedEntity): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]
Item: ( atcdabbr_other_AssignedEntity)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="string(@classCode) = ('PSN') or not(@classCode)">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von classCode MUSS 'PSN' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]">
      <extends rule="PN"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PN')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:PN" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_PersonNameCompilationG2M): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:family[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:family[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="count(hl7:given[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_PersonNameCompilationG2M): Element hl7:given[not(@nullFlavor)] ist mandatory [min 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:prefix
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:prefix">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:family[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:family[not(@nullFlavor)]">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:given[not(@nullFlavor)]
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:given[not(@nullFlavor)]">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.11
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:suffix
Item: (atcdabbr_other_PersonNameCompilationG2M)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:assignedPerson[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]/hl7:suffix">
      <extends rule="ENXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ENXP')">(atcdabbr_other_PersonNameCompilationG2M): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ENXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theAttValue"
           value="distinct-values(tokenize(normalize-space(@qualifier),'\s'))"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.11-2023-03-31T112005.html"
              test="not(@qualifier) or empty($theAttValue[not(. = (doc('include/voc-1.2.40.0.34.6.0.10.8-DYNAMIC.xml')/*/valueSet/conceptList/concept/@code))])">(atcdabbr_other_PersonNameCompilationG2M): Der Wert von qualifier MUSS gewählt werden aus Value Set '1.2.40.0.34.6.0.10.8' ELGA_EntityNamePartQualifier (DYNAMIC).</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.22
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization
Item: ( atcdabbr_other_AssignedEntity)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="string(@classCode) = ('ORG') or not(@classCode)">(atcdabbr_other_OrganizationCompilationWithName): Der Wert von classCode MUSS 'ORG' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(atcdabbr_other_OrganizationCompilationWithName): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_OrganizationCompilationWithName): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_OrganizationCompilationWithName): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="count(hl7:addr[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_OrganizationCompilationWithName): Element hl7:addr[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:id[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:id[not(@nullFlavor)]">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_other_OrganizationCompilationWithName): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:name[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:name[not(@nullFlavor)]">
      <extends rule="ON"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ON')">(atcdabbr_other_OrganizationCompilationWithName): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ON" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:telecom[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:telecom[not(@nullFlavor)]">
      <extends rule="TEL.AT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabbr_other_OrganizationCompilationWithName): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="@value">(atcdabbr_other_OrganizationCompilationWithName): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="not(@value) or string-length(@value)&gt;0">(atcdabbr_other_OrganizationCompilationWithName): Attribute @value MUSS vom Datentyp 'st' sein  - '<value-of select="@value"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="not(@use) or string-length(@use) &gt; 0">(atcdabbr_other_OrganizationCompilationWithName): Attribute @use MUSS vom Datentyp 'set_cs' sein  - '<value-of select="@use"/>'</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_AddressCompilation): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber)) or ((hl7:streetAddressLine or (hl7:streetName and hl7:houseNumber)) and not((hl7:streetAddressLine and hl7:streetName and hl7:houseNumber) or (hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber))))">(atcdabbr_other_AddressCompilation): Es muss entweder streetAddressLine oder streetName UND houseNumber angegeben
                werden.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetAddressLine) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetAddressLine kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetName) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetName kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:houseNumber) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:houseNumber kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:state) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:state kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:additionalLocator) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:additionalLocator kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:streetName
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:streetName">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:houseNumber
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:houseNumber">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:state
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:state">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="info"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="string-length(text()) = 3">(atcdabbr_other_AddressCompilation): Es wird EMPFOHLEN, den Staat im ISO 3 Ländercode anzugeben.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:responsibleParty[hl7:assignedEntity]/hl7:assignedEntity[not(@nullFlavor)][hl7:assignedPerson]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.8
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]
Item: (atcdabbr_header_EncounterLocation)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]"
         id="d11058e564-false-d11800e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.8-2021-02-19T110816.html"
              test="string(@typeCode) = ('LOC') or not(@typeCode)">(atcdabbr_header_EncounterLocation): Der Wert von typeCode MUSS 'LOC' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.8-2021-02-19T110816.html"
              test="count(hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]) &gt;= 1">(atcdabbr_header_EncounterLocation): Element hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.8-2021-02-19T110816.html"
              test="count(hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]) &lt;= 1">(atcdabbr_header_EncounterLocation): Element hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.8
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]
Item: (atcdabbr_header_EncounterLocation)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]"
         id="d11058e568-false-d11854e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.8-2021-02-19T110816.html"
              test="string(@classCode) = ('SDLOC') or not(@classCode)">(atcdabbr_header_EncounterLocation): Der Wert von classCode MUSS 'SDLOC' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.8-2021-02-19T110816.html"
              test="count(hl7:code[not(@nullFlavor)]) &gt;= 1">(atcdabbr_header_EncounterLocation): Element hl7:code[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.8-2021-02-19T110816.html"
              test="count(hl7:code[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_EncounterLocation): Element hl7:code[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.8-2021-02-19T110816.html"
              test="count(hl7:serviceProviderOrganization[not(@nullFlavor)]) &gt;= 1">(atcdabbr_header_EncounterLocation): Element hl7:serviceProviderOrganization[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.8-2021-02-19T110816.html"
              test="count(hl7:serviceProviderOrganization[not(@nullFlavor)]) &lt;= 1">(atcdabbr_header_EncounterLocation): Element hl7:serviceProviderOrganization[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.8
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:code[not(@nullFlavor)]
Item: (atcdabbr_header_EncounterLocation)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:code[not(@nullFlavor)]"
         id="d11058e572-false-d11899e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.8-2021-02-19T110816.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atcdabbr_header_EncounterLocation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.8-2021-02-19T110816.html"
              test="@displayName">(atcdabbr_header_EncounterLocation): Attribut @displayName MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.1.8-2021-02-19T110816.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_header_EncounterLocation): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.1.8
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]
Item: (atcdabbr_header_EncounterLocation)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="string(@classCode) = ('ORG') or not(@classCode)">(atcdabbr_other_OrganizationCompilationWithName): Der Wert von classCode MUSS 'ORG' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(atcdabbr_other_OrganizationCompilationWithName): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_OrganizationCompilationWithName): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_OrganizationCompilationWithName): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="count(hl7:addr[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_OrganizationCompilationWithName): Element hl7:addr[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_other_OrganizationCompilationWithName): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]">
      <extends rule="ON"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ON')">(atcdabbr_other_OrganizationCompilationWithName): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ON" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:telecom[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:telecom[not(@nullFlavor)]">
      <extends rule="TEL.AT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabbr_other_OrganizationCompilationWithName): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="@value">(atcdabbr_other_OrganizationCompilationWithName): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="not(@value) or string-length(@value)&gt;0">(atcdabbr_other_OrganizationCompilationWithName): Attribute @value MUSS vom Datentyp 'st' sein  - '<value-of select="@value"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.9-2021-02-19T133125.html"
              test="not(@use) or string-length(@use) &gt; 0">(atcdabbr_other_OrganizationCompilationWithName): Attribute @use MUSS vom Datentyp 'set_cs' sein  - '<value-of select="@use"/>'</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.9
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_other_OrganizationCompilationWithName)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(@use) or (string-length(@use) &gt; 0 and not(matches(@use,'\s')))">(atcdabbr_other_AddressCompilation): Attribute @use MUSS vom Datentyp 'cs' sein  - '<value-of select="@use"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="not(hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber)) or ((hl7:streetAddressLine or (hl7:streetName and hl7:houseNumber)) and not((hl7:streetAddressLine and hl7:streetName and hl7:houseNumber) or (hl7:streetAddressLine and (hl7:streetName or hl7:houseNumber))))">(atcdabbr_other_AddressCompilation): Es muss entweder streetAddressLine oder streetName UND houseNumber angegeben
                werden.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetAddressLine) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetAddressLine kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:streetName) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:streetName kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:houseNumber) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:houseNumber kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:postalCode[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:postalCode[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:city[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:city[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:state) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:state kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &gt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:country[not(@nullFlavor)]) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:country[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="count(hl7:additionalLocator) &lt;= 1">(atcdabbr_other_AddressCompilation): Element hl7:additionalLocator kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:streetAddressLine">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:streetName
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:streetName">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:houseNumber
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:houseNumber">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:postalCode[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:city[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:state
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:state">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:country[not(@nullFlavor)]">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="info"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="string-length(text()) = 3">(atcdabbr_other_AddressCompilation): Es wird EMPFOHLEN, den Staat im ISO 3 Ländercode anzugeben.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.25
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator
Item: (atcdabbr_other_AddressCompilation)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:componentOf[hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]]/hl7:encompassingEncounter[hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.5-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]]/hl7:location[hl7:healthCareFacility]/hl7:healthCareFacility[not(@nullFlavor)][hl7:serviceProviderOrganization]/hl7:serviceProviderOrganization[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/hl7:additionalLocator">
      <extends rule="ADXP"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.25-2023-04-13T132100.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ADXP')">(atcdabbr_other_AddressCompilation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ADXP" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:component[not(@nullFlavor)][hl7:structuredBody]
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:component[not(@nullFlavor)][hl7:structuredBody]"
         id="d42e1509-false-d12455e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:structuredBody[not(@nullFlavor)][hl7:component]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:structuredBody[not(@nullFlavor)][hl7:component] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:structuredBody[not(@nullFlavor)][hl7:component]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:structuredBody[not(@nullFlavor)][hl7:component] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:component[not(@nullFlavor)][hl7:structuredBody]/hl7:structuredBody[not(@nullFlavor)][hl7:component]
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->

   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:component[not(@nullFlavor)][hl7:structuredBody]/hl7:structuredBody[not(@nullFlavor)][hl7:component]"
         id="d42e1515-false-d13061e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@classCode) = ('DOCBODY') or not(@classCode)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von classCode MUSS 'DOCBODY' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@moodCode) = ('EVN') or not(@moodCode)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.23']]]) &gt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.23']]] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.23']]]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.23']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.4'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.11'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.6']]]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.4'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.11'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.6']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.8']]]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.8']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.2.1']]]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.2.1']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.23']]]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.23']]] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="count(hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.71']]]) &lt;= 1">(eimpf_document_UpdateImmunisierungsstatus): Element hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.71']]] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:component[not(@nullFlavor)][hl7:structuredBody]/hl7:structuredBody[not(@nullFlavor)][hl7:component]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.23']]]
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:component[not(@nullFlavor)][hl7:structuredBody]/hl7:structuredBody[not(@nullFlavor)][hl7:component]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.23']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:component[not(@nullFlavor)][hl7:structuredBody]/hl7:structuredBody[not(@nullFlavor)][hl7:component]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.4'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.11'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.6']]]
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:component[not(@nullFlavor)][hl7:structuredBody]/hl7:structuredBody[not(@nullFlavor)][hl7:component]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.4'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.11'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.6']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:component[not(@nullFlavor)][hl7:structuredBody]/hl7:structuredBody[not(@nullFlavor)][hl7:component]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.8']]]
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:component[not(@nullFlavor)][hl7:structuredBody]/hl7:structuredBody[not(@nullFlavor)][hl7:component]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.5'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.8']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:component[not(@nullFlavor)][hl7:structuredBody]/hl7:structuredBody[not(@nullFlavor)][hl7:component]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.2.1']]]
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:component[not(@nullFlavor)][hl7:structuredBody]/hl7:structuredBody[not(@nullFlavor)][hl7:component]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.2.1']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:component[not(@nullFlavor)][hl7:structuredBody]/hl7:structuredBody[not(@nullFlavor)][hl7:component]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.23']]]
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:component[not(@nullFlavor)][hl7:structuredBody]/hl7:structuredBody[not(@nullFlavor)][hl7:component]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.6'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.23']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.0.2
Context: /hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:component[not(@nullFlavor)][hl7:structuredBody]/hl7:structuredBody[not(@nullFlavor)][hl7:component]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.71']]]
Item: (eimpf_document_UpdateImmunisierungsstatus)
-->
   <rule fpi="RULC-1"
         context="/hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]/hl7:component[not(@nullFlavor)][hl7:structuredBody]/hl7:structuredBody[not(@nullFlavor)][hl7:component]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.71']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.2-2023-01-23T144821.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(eimpf_document_UpdateImmunisierungsstatus): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>
</pattern>
